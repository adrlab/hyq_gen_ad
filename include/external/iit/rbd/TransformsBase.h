/* CPYHDR { */
/*
 * Copyright 2012, 2013, 2014 Marco Frigerio, Istituto Italiano di Tecnologia,
 * all rights reserved.
 *
 * This file can be copied and distributed for educational or academic
 * purposes only. Distribution of modified versions of this file is
 * not permitted without explicit permission of the copyright holder.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 *
 * For further information please contact the author:
 *    marco.frigerio@iit.it
 */
/* } CPYHDR */

#ifndef IIT_RBD_TRANSFORMSBASE_H_
#define IIT_RBD_TRANSFORMSBASE_H_

#include "StateDependentMatrix.h"

namespace iit {
namespace rbd {


/**
 * A 4x4 specialization of StateDependentMatrix, to be used as a base class for
 * homogeneous transformation matrices that depend on a state variable.
 */
template<class State, class ActualMatrix>
class HomogeneousTransformBase : public StateDependentMatrix<State, 4, 4, ActualMatrix>
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

/**
 * A 6x6 specialization of StateDependentMatrix, to be used as a base class for
 * spatial transformation matrices that depend on a state variable.
 */
template<class State, class ActualMatrix>
class SpatialTransformBase : public StateDependentMatrix<State, 6, 6, ActualMatrix>
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

/**
 * A 6xCols specialization of StateDependentMatrix, to be used as a base class
 * for geometric Jacobians that depend on a state variable.
 */
template<class State, int Cols, class ActualMatrix>
class JacobianBase : public StateDependentMatrix<State, 6, Cols, ActualMatrix>
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};



}
}



#endif /* IIT_RBD_TRANSFORMSBASE_H_ */
