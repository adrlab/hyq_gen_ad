/*
 * CppADDoubleTrait.hpp
 *
 *  Created on: Nov 7, 2016
 *      Author: neunertm
 */

#ifndef INCLUDE_EXTERNAL_IIT_RBD_TRAITS_CPPADDOUBLETRAIT_H_
#define INCLUDE_EXTERNAL_IIT_RBD_TRAITS_CPPADDOUBLETRAIT_H_

#include <cppad/cppad.hpp>
#include <cppad/example/cppad_eigen.hpp>


namespace iit {
namespace rbd {

struct CppADDoubleTrait {

	typedef CppAD::AD<double> Scalar;

	inline static Scalar sin(const Scalar& x) { return CppAD::sin(x); }
	inline static Scalar cos(const Scalar& x) { return CppAD::cos(x); }

	template <int Dims>
	inline static Eigen::Matrix<Scalar, Dims, 1> solve(const Eigen::Matrix<Scalar, Dims, Dims>& A, const Eigen::Matrix<Scalar, Dims, 1>& b)
	{
		return A.inverse()*b;
	}

};

}
}

#endif /* INCLUDE_EXTERNAL_IIT_RBD_TRAITS_CPPADDOUBLETRAIT_H_ */
