/* CPYHDR { */
/*
 * Copyright 2012, 2013, 2014 Marco Frigerio, Istituto Italiano di Tecnologia,
 * all rights reserved.
 *
 * This file can be copied and distributed for educational or academic
 * purposes only. Distribution of modified versions of this file is
 * not permitted without explicit permission of the copyright holder.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 *
 * For further information please contact the author:
 *    marco.frigerio@iit.it
 */
/* } CPYHDR */

#ifndef _IIT_RBD_INERTIAMATRIX_H_
#define _IIT_RBD_INERTIAMATRIX_H_

#include "rbd.h"


namespace iit {
namespace rbd {

/**
 * Dense 6x6 matrix that represents the 6D spatial inertia tensor.
 * See chapther 2 of Featherstone's "Rigid body dynamics algorithms".
 */
template <typename TRAIT>
class InertiaMatrixDenseTpl : public Matrix66dTpl<typename TRAIT::Scalar> {
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

private:
	typedef typename TRAIT::Scalar SCALAR;

    typedef Matrix66dTpl<SCALAR> Base;
    typedef Matrix33dTpl<SCALAR> Mat33d;
    typedef typename Eigen::Block<const Base,3,3> Block33_const;
    typedef typename Eigen::Block<Base,3,3> Block33_t;
    typedef Vector3dTpl<SCALAR> Vec3d;

public:
    template<typename OtherDerived>
    InertiaMatrixDenseTpl& operator= (const Eigen::MatrixBase<OtherDerived>& other);

    InertiaMatrixDenseTpl();
    /**
     * See fill()
     */
    InertiaMatrixDenseTpl(SCALAR m, const Vec3d& com, const Mat33d& I);

public:
    /**
     * Sets this 6x6 inertia tensor according to the given inertia properties.
     * All the values (ie the COM and the 3x3 tensor) must be expressed in the
     * same reference frame.
     *
     * No consistency checks are performed (Note: possibly changing in future).
     *
     * \param mass the total mass of the body
     * \param com the 3D vector with the position of the center of mass
     * \param tensor the classical 3x3 inertia tensor; this parameter should be
     *    expressed in the same coordinate frame as the center-of-mass vector.
     *    In other words, it is NOT treated as the inertia tensor with respect
     *    to a frame with origin in the center-of-mass, and the parallel axis
     *    theorem is NOT applied. The given tensor is copied as it is, in the
     *    appropriate 3x3 sub-block of this spatial tensor.
     */
    void fill(SCALAR m, const Vec3d& com, const Mat33d& tensor);

    iit::rbd::VelocityVectorTpl<SCALAR> solveCustom(const iit::rbd::ForceVectorTpl<SCALAR>& force);

    /** \name Components getters **/
    ///@{
    /**
     * @return the current value of the mass of the rigid body modeled by this
     * tensor
     */
    SCALAR getMass() const;
    /**
     * @return the position of the center-of-mass of the rigid body modeled by
     *   this spatial tensor
     */
    Vec3d getCOM() const;
    /**
     * @return the 3x3 block of this spatial tensor which corresponds to the
     *   sole rotational inertia, that is, the classical inertia tensor
     */
    const Block33_const get3x3Tensor() const;
    ///@}

    /** \name Modifiers **/
    ///@{
    /**
     * Scales the whole tensor according to the new value of the mass.
     *
     * The changes guarantee that the matrix remains positive definite (assuming
     * it was so before).
     *
     * This method does NOT change the center-of-mass property, while it does
     * change the moments of inertia. Intuitively, calling this method corresponds
     * to changing the mass-density of the body leaving its size and geometry
     * untouched.
     *
     * @param newMass the new value of the mass (always expressed in Kilograms);
     *    it MUST be positive, no checks are performed
     */
    void changeMass(SCALAR m);
    /**
     * Changes the position of the Center-Of-Mass of the rigid body modeled by
     * this tensor.
     *
     * In addition to the two off-diagonal 3x3 blocks, this method also modifies
     * the 3x3 block that corresponds to the classical inertia tensor, to keep
     * it consistent with the position of the center of mass. It does not change
     * the mass property. Cfr. chapter 2 of Featherstone's book on rigid body
     * dynamics algorithms.
     * TODO show some equations
     *
     * @param newcom a 3D vector specifying the position of the center of mass,
     *   expressed in meters
     */
    void changeCOM(const Vec3d& newcom);
    /**
     * Simply sets the 3x3 block that corresponds to the classical rotational
     * inertia
     * @param tensor the new 3x3 rotational inertia tensor
     */
    void changeRotationalInertia(const Mat33d& tensor);
    ///@}
protected:
    void setTheFixedZeros();

    template<typename Vector>
    void setSkewSymmetricBlock(
            const Eigen::MatrixBase<Vector>& v,
            Block33_t block);
private:
    void set(SCALAR m, const Vec3d& com, const Mat33d& I);

};

using InertiaMatrixDense = InertiaMatrixDenseTpl<double>;

class InertiaMatrixSparse : public SparseMatrixd {
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    typedef SparseMatrixd Base;
};


#define block33 this->template block<3,3>
#define data    (this->operator())


/* I do not care here about the creation of temporaries, thus I use const
 * refs to actual Eigen matrices, rather than the common base of the
 * matrix expression. If you haven't read Eigen docs, this comment is
 * completely obscure!
 */

template <typename TRAIT>
inline InertiaMatrixDenseTpl<TRAIT>::InertiaMatrixDenseTpl() : Base() {
    setTheFixedZeros();
}

/**
 * Initializes this 6x6 tensor according to the given inertia parameters.
 * \see fill()
 */
template <typename TRAIT>
inline InertiaMatrixDenseTpl<TRAIT>::InertiaMatrixDenseTpl(
        SCALAR mass, const Vec3d& cogPosition, const Mat33d& tensor)
: Base()
{
    setTheFixedZeros();
    set(mass, cogPosition, tensor);
}

template <typename TRAIT>
inline void InertiaMatrixDenseTpl<TRAIT>::fill(SCALAR mass, const Vec3d& comPosition,
        const Mat33d& tensor)
{
    set(mass, comPosition, tensor);
}

template <typename TRAIT>
inline typename InertiaMatrixDenseTpl<TRAIT>::SCALAR InertiaMatrixDenseTpl<TRAIT>::getMass() const
{
    return data(LX,LX);
}

template <typename TRAIT>
inline typename InertiaMatrixDenseTpl<TRAIT>::Vec3d InertiaMatrixDenseTpl<TRAIT>::getCOM() const
{
    return Vec3d(
            data(AZ,LY)/data(LX,LX), // X coordinate of the COM
            data(AX,LZ)/data(LX,LX), // Y
            data(AY,LX)/data(LX,LX));// Z
}

template <typename TRAIT>
inline const typename InertiaMatrixDenseTpl<TRAIT>::Block33_const InertiaMatrixDenseTpl<TRAIT>::get3x3Tensor() const
{
    return block33(AX,AX);
}

template <typename TRAIT>
inline void InertiaMatrixDenseTpl<TRAIT>::changeMass(SCALAR newMass) {
    // Note the use of indices AX and hard-coded 0, to make it independent from
    //  the convention angular/linear
    this->block<3,6>(AX,0) *= newMass/getMass();
    block33(LX,AX) = block33(AX,LX).transpose();
    data(LX,LX) = data(LY,LY) = data(LZ,LZ) = newMass;
}

template <typename TRAIT>
inline void InertiaMatrixDenseTpl<TRAIT>::changeCOM(const Vec3d& newcom)
{
    // Update the angular-linear block according to the new COM position
    setSkewSymmetricBlock(  getMass() * newcom, block33(AX,LX));

    // Correct the 3x3 tensor. Use the block(AX,LX) which reflects the new COM
    //  and the block(LX,AX) which instead is still based on the previous value.
    //     I = I - m(cx)(cx)^T + m(c'x)(c'x)^T
    //  where cx is the skew symmetric matrix for the old COM vector, while
    //  c'x is the one for the new COM vector.
    block33(AX,AX) += (
            ( block33(AX,LX) * block33(AX,LX).transpose() ) -
            ( block33(LX,AX).transpose() * block33(LX,AX) )
                ) / getMass();
    // Update the linear-angular block
    block33(LX,AX) = block33(AX,LX).transpose();
    // alternatively:
    // setSkewSymmetricBlock( - getMass() * newcom, block33(LX,AX));
}

template <typename TRAIT>
inline void InertiaMatrixDenseTpl<TRAIT>::changeRotationalInertia(const Mat33d& tensor)
{
    block33(AX,AX) = tensor;
}

template <typename TRAIT>
inline void InertiaMatrixDenseTpl<TRAIT>::set(
        SCALAR mass,
        const Vec3d& comPosition,
        const Mat33d& tensor)
{
    block33(AX,AX) = tensor.template cast<SCALAR>();// + mass * comCrossMx * comCrossMx.transpose();
    setSkewSymmetricBlock(  SCALAR(mass) * comPosition.template cast<SCALAR>(), block33(AX,LX));
    setSkewSymmetricBlock(- SCALAR(mass) * comPosition.template cast<SCALAR>(), block33(LX,AX));
    data(LX,LX) = data(LY,LY) = data(LZ,LZ) = SCALAR(mass);
}


template <typename TRAIT>
template <typename OtherDerived>
inline InertiaMatrixDenseTpl<TRAIT>& InertiaMatrixDenseTpl<TRAIT>::operator=
        (const Eigen::MatrixBase<OtherDerived>& other)
{
    this->Base::operator=(other);
    return *this;
}

template <typename TRAIT>
inline void InertiaMatrixDenseTpl<TRAIT>::setTheFixedZeros()
{
    block33(LX,LX).setZero(); // the diagonal won't be zero, but who cares
    data(AX,LX) = data(AY,LY) = data(AZ,LZ) =
            data(LX,AX) = data(LY,AY) = data(LZ,AZ) = 0;
}

template <typename TRAIT>
template <typename Vector>
inline void InertiaMatrixDenseTpl<TRAIT>::setSkewSymmetricBlock(
        const Eigen::MatrixBase<Vector>& v, Block33_t block)
{
    block(X,Y) = - ( block(Y,X) = v(Z) );
    block(Z,X) = - ( block(X,Z) = v(Y) );
    block(Y,Z) = - ( block(Z,Y) = v(X) );
}


#undef block33
#undef data

}
}


#endif /* INERTIAMATRIX_H_ */
