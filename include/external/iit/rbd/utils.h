/* CPYHDR { */
/*
 * Copyright 2012, 2013, 2014 Marco Frigerio, Istituto Italiano di Tecnologia,
 * all rights reserved.
 *
 * This file can be copied and distributed for educational or academic
 * purposes only. Distribution of modified versions of this file is
 * not permitted without explicit permission of the copyright holder.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 *
 * For further information please contact the author:
 *    marco.frigerio@iit.it
 */
/* } CPYHDR */
#ifndef IIT_RBD_UTILS_H_
#define IIT_RBD_UTILS_H_

#include <cmath>
#include <iostream>

#include "rbd.h"

namespace iit {
namespace rbd {

class Utils {
    public:
        struct InertiaMoments {
            double ixx; double iyy; double izz; double ixy; double ixz; double iyz;
        };

        /**
         * Coefficient-wise, unary operator that evaluates to zero for each element
         * of the matrix expression whose absolute value is below a threshold.
         * Useful for printing matrices, approximating with 0 small coefficients.
         */
        template <typename Scalar>
        struct CwiseAlmostZeroOp {
            CwiseAlmostZeroOp(const Scalar& threshold) : thresh(threshold) {}
            const Scalar operator()(const Scalar& x) const {
                return std::abs(x) < thresh ? 0 : x;
            }
            private:
                Scalar thresh;
        };
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	template <typename SCALAR>
    static Matrix33dTpl<SCALAR> buildInertiaTensor(
            SCALAR Ixx, SCALAR Iyy, SCALAR Izz,
            SCALAR Ixy, SCALAR Ixz, SCALAR Iyz)
    {
    	Matrix33dTpl<SCALAR> I;
        I <<  Ixx, -Ixy, -Ixz,
             -Ixy,  Iyy, -Iyz,
             -Ixz, -Iyz,  Izz;
        return I;
    }

    static Matrix33d buildCrossProductMatrix(const Vector3d& in) {
        Matrix33d out;
        out <<  0   , -in(2),  in(1),
               in(2),   0   , -in(0),
              -in(1),  in(0),   0;
        return out;
    }

    // Put implementation here in header even if it is not inline, since
    //  this is a template function
    /**
     * Suppose B is rotated respect to A by rx, than ry, than rz.
     * Fills the last parameter with the 3x3 rotation matrix A --> B ('B_X_A')
     */
    template <typename Derived>
    static void fillAsRotationMatrix(double rx, double ry, double rz,
            const Eigen::MatrixBase<Derived>& mx)
    {
        eigen_assert(mx.rows() == 3  &&  mx.cols() == 3);

        static double cx;
        static double sx;
        static double cy;
        static double sy;
        static double cz;
        static double sz;
        cx = std::cos(rx);
        sx = std::sin(rx);
        cy = std::cos(ry);
        sy = std::sin(ry);
        cz = std::cos(rz);
        sz = std::sin(rz);

        const_cast<Eigen::MatrixBase<Derived>&>(mx)
                   << cy*cz, cx*sz+sx*sy*cz, sx*sz-cx*sy*cz,
                     -cy*sz, cx*cz-sx*sy*sz, cx*sy*sz+sx*cz,
                      sy,   -sx*cy,         cx*cy;
    }

    /**
     * Suppose B is rotated respect to A by rx, than ry, than rz.
     * \return the 3x3 rotation matrix A --> B ('B_X_A')
     */
    static Matrix33d buildRotationMatrix(double rx, double ry, double rz) {
        double cx = std::cos(rx);
        double sx = std::sin(rx);
        double cy = std::cos(ry);
        double sy = std::sin(ry);
        double cz = std::cos(rz);
        double sz = std::sin(rz);
        Matrix33d ret;
        ret << cy*cz, cx*sz+sx*sy*cz, sx*sz-cx*sy*cz,
                -cy*sz, cx*cz-sx*sy*sz, cx*sy*sz+sx*cz,
                 sy,   -sx*cy,         cx*cy;
        return ret;
    }


    template <typename SCALAR>
    static void fillAsMotionCrossProductMx(const Column6dTpl<SCALAR>& v, Matrix66dTpl<SCALAR>& mx) {
        fillAsCrossProductMatrix(v.template block<3,1>(0,0), mx.template block<3,3>(0,0));
        mx.template block<3,3>(0,3).setZero();
        fillAsCrossProductMatrix(v.template block<3,1>(3,0), mx.template block<3,3>(3,0));
        mx.template block<3,3>(3,3) = mx.template block<3,3>(0,0);
    }

    template <typename SCALAR>
    static void fillAsForceCrossProductMx(const Column6dTpl<SCALAR>& v, Matrix66dTpl<SCALAR>& mx) {
        fillAsCrossProductMatrix(v.template block<3,1>(0,0), mx.template block<3,3>(0,0));
        fillAsCrossProductMatrix(v.template block<3,1>(3,0), mx.template block<3,3>(0,3));
        mx.template block<3,3>(3,0).setZero();
        mx.template block<3,3>(3,3) = mx.template block<3,3>(0,0);
    }

    template <typename Derived, typename Derived2>
    static void fillAsCrossProductMatrix(const Eigen::MatrixBase<Derived2>& in, const Eigen::MatrixBase<Derived>& mx) {
        eigen_assert(mx.rows() == 3  &&  mx.cols() == 3);
        typedef typename Eigen::MatrixBase<Derived2>::Scalar SCALAR;
        const_cast<Eigen::MatrixBase<Derived>&>(mx)
            <<  static_cast<SCALAR>(0.0), -in(2),  in(1),
               in(2),   static_cast<SCALAR>(0.0)   , -in(0),
              -in(1),  in(0),   static_cast<SCALAR>(0.0);
    }

    template <typename Derived>
    static const Eigen::Block<const Derived,3,1> positionVector(const Eigen::MatrixBase<Derived>& homT) {
        eigen_assert(homT.rows() == 4  &&  homT.cols() == 4); // weak way to check if it is a homogeneous transform
        return homT.template block<3,1>(0,3);
    }
    template <typename Derived>
    static const Eigen::Block<const Derived,3,3> rotationMx(const Eigen::MatrixBase<Derived>& homT) {
        eigen_assert(homT.rows() == 4  &&  homT.cols() == 4); // weak way to check if it is a homogeneous transform
        return homT.template block<3,3>(0,0);
    }
    template <typename Derived>
    static const Eigen::Block<const Derived,3,1> zAxis(const Eigen::MatrixBase<Derived>& homT) {
        eigen_assert(homT.rows() == 4  &&  homT.cols() == 4); // weak way to check if it is a homogeneous transform
        return homT.template block<3,1>(0,2);
    }

    /**
     * Applies a roto-translation to the given 3D vector.
     * This function is provided for convenience, since a direct matrix product
     * between a 4x4 matrix and a 3x1 vector is obviously not possible. It should
     * also be slightly more efficient than taking the homogeneous representation
     * of the 3D vector (4x1) and then compute the matrix product.
     *
     * \param homT a 4x4 homogeneous coordinate transform encoding the rotation
     *        and the translation
     * \param vect3d the 3x1 vector to be transformed
     * \return the 3D vector that results from the rotation plus translation
     */
    template <typename Derived, typename Other>
    static Vector3d transform(
            const Eigen::MatrixBase<Derived>& homT,
            const Eigen::MatrixBase<Other>& vect3d)
    {
        eigen_assert(homT.rows() == 4  &&  homT.cols() == 4); // weak way to check if it is a homogeneous transform
        eigen_assert(vect3d.rows() == 3  &&  vect3d.cols() == 1); // check if it is a 3d column
        return rotationMx(homT) * vect3d + positionVector(homT);
    }

};


}
}

#endif /* IIT_RBD_UTILS_H_ */
