#ifndef IIT_ROBOGEN__HYQ_TRAITS_H_
#define IIT_ROBOGEN__HYQ_TRAITS_H_

#include "declarations.h"
#include "transforms.h"
#include "inverse_dynamics.h"
#include "forward_dynamics.h"
#include "jsim.h"

namespace iit {
namespace HyQ {

struct Traits {
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    typedef typename HyQ::JointState JointState;

    typedef typename HyQ::JointIdentifiers JointID;
    typedef typename HyQ::LinkIdentifiers  LinkID;

    typedef typename HyQ::HomogeneousTransforms HomogeneousTransforms;
    typedef typename HyQ::MotionTransforms MotionTransforms;
    typedef typename HyQ::ForceTransforms ForceTransforms;

    typedef typename HyQ::dyn::ForwardDynamics FwdDynEngine;
    typedef typename HyQ::dyn::InverseDynamics InvDynEngine;
    typedef typename HyQ::dyn::JSIM JSIM;

    static const int joints_count = HyQ::jointsCount;
    static const int links_count  = HyQ::linksCount;
    static const bool floating_base = true;

    static inline const JointID* orderedJointIDs();
    static inline const LinkID*  orderedLinkIDs();
};


inline const Traits::JointID*  Traits::orderedJointIDs() {
    return HyQ::orderedJointIDs;
}
inline const Traits::LinkID*  Traits::orderedLinkIDs() {
    return HyQ::orderedLinkIDs;
}

}
}

#endif
