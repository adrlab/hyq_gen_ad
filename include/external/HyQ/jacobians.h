#ifndef HYQ_JACOBIANS_H_
#define HYQ_JACOBIANS_H_

#include <iit/rbd/TransformsBase.h>
#include <iit/rbd/traits/DoubleTrait.h>

#include "declarations.h"
#include "transforms.h"
#include "kinematics_parameters.h"

namespace iit {
namespace HyQ {

template<typename SCALAR, int COLS, class M>
class JacobianT : public iit::rbd::JacobianBase<JointStateTpl<SCALAR>, COLS, M>
{};

/**
 *
 */
template <typename TRAIT>
class JacobiansTpl {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW

		// specialize class to trait / scalar type
		typedef typename TRAIT::Scalar SCALAR;

		typedef JointStateTpl<SCALAR> JState;

        class Type_fr_trunk_J_LF_foot : public JacobianT<SCALAR, 3, Type_fr_trunk_J_LF_foot>
        {
        public:
            EIGEN_MAKE_ALIGNED_OPERATOR_NEW
            Type_fr_trunk_J_LF_foot();
            const Type_fr_trunk_J_LF_foot& update(const JState&);
        protected:
        };
        
        class Type_fr_trunk_J_RF_foot : public JacobianT<SCALAR, 3, Type_fr_trunk_J_RF_foot>
        {
        public:
            EIGEN_MAKE_ALIGNED_OPERATOR_NEW
            Type_fr_trunk_J_RF_foot();
            const Type_fr_trunk_J_RF_foot& update(const JState&);
        protected:
        };
        
        class Type_fr_trunk_J_LH_foot : public JacobianT<SCALAR, 3, Type_fr_trunk_J_LH_foot>
        {
        public:
            EIGEN_MAKE_ALIGNED_OPERATOR_NEW
            Type_fr_trunk_J_LH_foot();
            const Type_fr_trunk_J_LH_foot& update(const JState&);
        protected:
        };
        
        class Type_fr_trunk_J_RH_foot : public JacobianT<SCALAR, 3, Type_fr_trunk_J_RH_foot>
        {
        public:
            EIGEN_MAKE_ALIGNED_OPERATOR_NEW
            Type_fr_trunk_J_RH_foot();
            const Type_fr_trunk_J_RH_foot& update(const JState&);
        protected:
        };
        
    public:
        JacobiansTpl();
        void updateParameters();
    public:
        Type_fr_trunk_J_LF_foot fr_trunk_J_LF_foot;
        Type_fr_trunk_J_RF_foot fr_trunk_J_RF_foot;
        Type_fr_trunk_J_LH_foot fr_trunk_J_LH_foot;
        Type_fr_trunk_J_RH_foot fr_trunk_J_RH_foot;

    protected:

};
using Jacobians = JacobiansTpl<rbd::DoubleTrait>;

#include "jacobians.impl.h"

}
}

#endif
