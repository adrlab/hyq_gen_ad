/*
 * EEKinematics.h
 *
 *  Created on: Jul 19, 2016
 *      Author: neunertm
 */


template <typename SCALAR>
void EEKinematicsTpl<SCALAR>::getEEPosInBase(
        Vector3AD& B_eePos,
        const LEG_ID& LEG_ID,
		const JointAnglesAD& jointAngles)
{
    switch(LEG_ID)
    {
        case LF:
        {
            B_eePos = transforms_->fr_trunk_X_LF_foot(jointAngles).template block<3,1>(0,3);
            break;
        }
        case RF:
        {
            B_eePos = transforms_->fr_trunk_X_RF_foot(jointAngles).template block<3,1>(0,3);
            break;
        }
        case LH:
        {
            B_eePos = transforms_->fr_trunk_X_LH_foot(jointAngles).template block<3,1>(0,3);
            break;
        }
        case RH:
        {
            B_eePos = transforms_->fr_trunk_X_RH_foot(jointAngles).template block<3,1>(0,3);
            break;
        }
        default:
            throw std::runtime_error("invalid leg Id");
    }
}


template <typename SCALAR>
void EEKinematicsTpl<SCALAR>::getEEPosInWorld(
        Vector3AD& W_eePos,
        const LEG_ID& LEG_ID,
		const RBDStateHyQAD& state)
{
        Vector3AD B_eePos;
        JointAnglesAD jointAngles = state.getJointPositions();
        getEEPosInBase(B_eePos, LEG_ID, jointAngles);

        W_eePos = state.getBasePositionW() + state.getBaseRotationMatrix().transpose() * B_eePos;
}

template <typename SCALAR>
void EEKinematicsTpl<SCALAR>::getEEVelInBase(
        Vector3AD& B_eeVel,
        const LEG_ID& LEG_ID,
		const RBDStateHyQAD& state)
{
    Vector3AD B_eePos;
    JointAnglesAD jointAngles = state.getJointPositions();
    getEEPosInBase(B_eePos, LEG_ID, jointAngles);

    switch(LEG_ID)
    {
        case LF:
        {
            B_eeVel = (jacobians_->fr_trunk_J_LF_foot(jointAngles)*state.getJointVelocities().template segment<3>(3*LEG_ID)).template bottomRows<3>();
            break;
        }
        case RF:
        {
            B_eeVel = (jacobians_->fr_trunk_J_RF_foot(jointAngles)*state.getJointVelocities().template segment<3>(3*LEG_ID)).template bottomRows<3>();
            break;
        }
        case LH:
        {
            B_eeVel = (jacobians_->fr_trunk_J_LH_foot(jointAngles)*state.getJointVelocities().template segment<3>(3*LEG_ID)).template bottomRows<3>();
            break;
        }
        case RH:
        {
            B_eeVel = (jacobians_->fr_trunk_J_RH_foot(jointAngles)*state.getJointVelocities().template segment<3>(3*LEG_ID)).template bottomRows<3>();
            break;
        }
        default:
            throw std::runtime_error("invalid leg Id");
    }

    B_eeVel += state.getBaseLinearVelocityB() - B_eePos.cross(state.getBaseLocalAngularVelocityB());
}

template <typename SCALAR>
void EEKinematicsTpl<SCALAR>::getEEVelInWorld(
        Vector3AD& W_eeVel,
        const LEG_ID& LEG_ID,
		const RBDStateHyQAD& state)
{
    Vector3AD B_eeVel;
    getEEVelInBase(B_eeVel, LEG_ID, state);

    W_eeVel =  state.getBaseRotationMatrix().transpose() * B_eeVel;
}

template <typename SCALAR>
void EEKinematicsTpl<SCALAR>::mapEEForcesToExtForces(
		const RBDStateHyQAD& state,
		const std::array<Vector3AD, N_LEGS>& eeForcesW,
		ExtForces& extForces
)
{
	// Vector from knee joint to foot. This is the lever arm for a torque computation that will follow.
	//todo remove hardcoding
	Vector3AD leverKneeEE;
	leverKneeEE << 0.33, 0.0, 0.0;

	for (size_t i=0; i<N_LEGS; i++)
	{
		// express forces in base frame (rotate from world to base)
		Vector3AD eeForceB = state.getBaseRotationMatrix()*eeForcesW[i];

		Eigen::Matrix3d R_B_LL;

		// get the rotation from the base to the link it is acting on
		switch(i)
		{
			case LF:
				R_B_LL = transforms_->fr_trunk_X_LF_lowerlegCOM(state.getJointPositions()).template topLeftCorner<3,3>();
				break;
			case RF:
				R_B_LL = transforms_->fr_trunk_X_RF_lowerlegCOM(state.getJointPositions()).template topLeftCorner<3,3>();
				break;
			case LH:
				R_B_LL = transforms_->fr_trunk_X_LH_lowerlegCOM(state.getJointPositions()).template topLeftCorner<3,3>();
				break;
			case RH:
				R_B_LL = transforms_->fr_trunk_X_RH_lowerlegCOM(state.getJointPositions()).template topLeftCorner<3,3>();
				break;
			default:
				std::runtime_error("Transform lookup failed.");
				break;
		}

		Vector3AD eeForceLL = R_B_LL.transpose()*eeForceB;

		// the force at the foot introduces a torque in the knee
		Vector3AD eeTorqueLL = leverKneeEE.cross(eeForceLL);

		extForces[static_cast<iit::HyQ::LinkIdentifiers>(3*(i+1))] << eeTorqueLL, eeForceLL;
	}
}
