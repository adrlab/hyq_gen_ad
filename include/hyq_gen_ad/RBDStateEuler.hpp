/*! \file 		RBDStateEuler.hpp
 *	\brief		Class for the RigidBodyDynamics using Euler representation
 *  \author	    Michael Neunert
 */

#ifndef INCLUDE_HYQ_GEN_AD_RBDSTATEEULER_HPP_
#define INCLUDE_HYQ_GEN_AD_RBDSTATEEULER_HPP_

#include <Eigen/Core>

namespace hyq_gen_ad {

/**
 * @brief The State of the RBD (representing base using Euler angles)
 *
 * This state contains the following items in this exact order
 * - Base orientation as Euler Angles (world frame)
 * - Base position (world frame)
 * - Joint angles
 * - Base local angular velocity (local base frame)
 * - Base linear velocity (local base frame)
 * - Joint velocities
 */
template <size_t NJOINTS, typename SCALAR = double>
class RBDStateEuler
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	/**
	 * @brief An enumeration for the system dimensions
	 */
	/*! System Dimensions */
	enum DIMENSIONS {
		BASE_DOF = 6, /*!< Base DoF: 6 */
		BASE_DIM = 2*BASE_DOF, /*!< Base State Dimensionality: 12 */
		STATE_DIM = BASE_DIM + 2*NJOINTS /*!< Full State Dimensionality: 12 + 2*nJoints */
	};

	/**
	 * @brief An enumeration for state indices
	 */
	/*! State Indices */
	enum INDECES {
		BASE_ORIENTATION = 0, /*!< 0 */
		BASE_POSITION = 3, /*!< 3 */
		JOINT_POSITIONS = BASE_DOF, /*!< 3 + 3 */
		BASE_ANGULAR_VELOCITY = JOINT_POSITIONS + NJOINTS, /*!< 3 + 3 + nJoints = 6 + nJoints */
		BASE_LINEAR_VELOCITY = BASE_ANGULAR_VELOCITY + 3, /*!< 3 + 3 + nJoints + 3 = 9 + nJoints */
		JOINT_VELOCITIES = BASE_LINEAR_VELOCITY + 3 /*!< 3 + 3 + nJoints + 3 + 3 = 12 + nJoints */
	};

	typedef Eigen::Matrix<SCALAR, Eigen::Dynamic, 1> state_vector_euler_t; /*!< Typedef for the underlying Eigen Vector */

	typedef Eigen::VectorBlock<state_vector_euler_t, 3> vector3_block_t; /*!< A 3D block (reference) */
	typedef Eigen::VectorBlock<state_vector_euler_t, NJOINTS> jointvector_block_t; /*!< A joint-sized block (reference) */
	typedef Eigen::VectorBlock<const state_vector_euler_t, 3> vector3_const_block_t; /*!< A 3D const block (const reference) */
	typedef Eigen::VectorBlock<const state_vector_euler_t, NJOINTS> jointvector_const_block_t; /*!< A joint-sized const block (const reference) */

	typedef Eigen::Matrix<SCALAR, 3, 3> rotation_matrix_t; /*!< A 3x3 rotation matrix */
	typedef Eigen::Matrix<SCALAR, 3, 1> vector3_t; /*!< A 3D vector */
	typedef Eigen::Matrix<SCALAR, 6, 1> vector6_t; /*!< A 6D Pluecker/motion vector*/

	/**
	 * @brief Default constructor
	 */
	RBDStateEuler() :
		state_(static_cast<int>(STATE_DIM))	{
		state_.setZero();
		initializeGravity();
	}

	/**
	 * @brief Constructor from Eigen Vector
	 * @param[in] state The Eigen vector of states
	 *
	 */
	RBDStateEuler(const state_vector_euler_t& state) {
		if (state.rows() != STATE_DIM)
			throw std::runtime_error("RBDStateEuler constructor dimension mismatch.");

		state_ = state;
		initializeGravity();
	}

	/**
	 * @brief Get the orientation of the base
	 * @return A 3D vector block (reference!) representing the XYZ orientation of the base
	 */
	vector3_block_t getBaseOrientationXyz() { return state_.template segment<3>(BASE_ORIENTATION); }
	vector3_const_block_t getBaseOrientationXyz() const { return state_.template segment<3>(BASE_ORIENTATION); }

	/**
	 * @brief Get the position of the base in world frame
	 * @return A 3D vector block (reference!) representing position of the base
	 */
	vector3_block_t getBasePositionW() { return state_.template segment<3>(BASE_POSITION); }
	vector3_const_block_t getBasePositionW() const { return state_.template segment<3>(BASE_POSITION); }

	/**
	 * @brief Get the angular velocity of the base in base frame
	 * @return A 3D vector block (reference!) representing base angular velocity
	 */
	vector3_block_t getBaseLocalAngularVelocityB() { return state_.template segment<3>(BASE_ANGULAR_VELOCITY); }
	vector3_const_block_t getBaseLocalAngularVelocityB() const { return state_.template segment<3>(BASE_ANGULAR_VELOCITY); }

	/**
	 * @brief Get the linear velocity of the base in base frame
	 * @return A 3D vector block (reference!) representing base linear velocity
	 */
	vector3_block_t getBaseLinearVelocityB() { return state_.template segment<3>(BASE_LINEAR_VELOCITY); }
	vector3_const_block_t getBaseLinearVelocityB() const { return state_.template segment<3>(BASE_LINEAR_VELOCITY); }


	/**
	 * @brief Compute the base velocity as 6D Pluecker vectors
	 * @return A 6D spatial vector representing base velocities
	 */
	vector6_t convertBaseVelocityBPluecker() const {
		vector6_t velocityB;
		velocityB << getBaseLocalAngularVelocityB(), getBaseLinearVelocityB();
		return velocityB;
	}

	/**
	 * @brief Get joint position
	 * @return A vector block (reference) of joint positions
	 */
	jointvector_block_t getJointPositions() { return state_.template segment<NJOINTS>(JOINT_POSITIONS); }
	jointvector_const_block_t getJointPositions() const { return state_.template segment<NJOINTS>(JOINT_POSITIONS); }

	/**
	 * @brief Get joint velocities
	 * @return A vector block (reference) of joint velocities
	 */
	jointvector_block_t getJointVelocities() { return state_.template segment<NJOINTS>(JOINT_VELOCITIES); }
	jointvector_const_block_t getJointVelocities() const { return state_.template segment<NJOINTS>(JOINT_VELOCITIES); }

	/**
	 * @brief Get the full state as an Eigen vector as internally saved
	 * @return The underlying dynamic-size Eigen vector
	 */
	state_vector_euler_t& eigen() { return state_; }
	const state_vector_euler_t& eigen() const { return state_; }

	/**
	 * @brief Get B_X_W rotation matrix
	 * @return The rotation matrix B_X_W
	 *
	 * This computes a rotation matrix that converts quantities expressed in
	 * the world frame to the base frame. Example
	 * RBDState state;
	 * Eigen::Vector3d gravity_W(0.0, 0.0, -9.81);
	 * Eigen::Vector3d gravity_B = state.getBaseRotationMatrix()*gravity_W;
	 */
	rotation_matrix_t getBaseRotationMatrix() const	{
		auto eulerAngles = getBaseOrientationXyz();

		// inputs are the intrinsic rotation angles in RADIANS
		const SCALAR& alpha = eulerAngles(0);
		const SCALAR& beta  = eulerAngles(1);
		const SCALAR& gamma = eulerAngles(2);

		#ifndef ROBCOGEN_AD_NAMESPACE
		#define ROBCOGEN_AD_NAMESPACE std
		#endif
		using ROBCOGEN_AD_NAMESPACE::sin;
		using ROBCOGEN_AD_NAMESPACE::cos;

		// for autodiff...
		SCALAR zero = SCALAR(0.0);
		SCALAR one = SCALAR(1.0);

		rotation_matrix_t Rx, Ry, Rz;
		Rx << one, zero, zero, zero,                    cos(-alpha), -sin(-alpha),        zero, sin(-alpha), cos(-alpha);
		Ry << cos(-beta), zero, sin(-beta),        zero, one, zero,                         -sin(-beta), zero, cos(-beta);
		Rz << cos(-gamma), -sin(-gamma), zero,    sin(-gamma), cos(-gamma), zero,        zero, zero, one;

		return Rz*Ry*Rx;
	}

	/**
	 * @brief Get gravity vector expressed in base frame
	 * @return The 3D gravity vector in base frame
	 */
	vector3_t computeGravityB() const {
		return getBaseRotationMatrix()*gravityW_;
	}

	/**
	 * @brief Get gravity motion vector
	 * @return The 6D motion vector representing gravity
	 */
	vector6_t computeGravityBPluecker() const {
		vector6_t gravity6D(vector6_t::Zero());
		gravity6D.template bottomRows<3>() = getBaseRotationMatrix()*gravityW_;
		return gravity6D;
	}

private:

	void initializeGravity() {
		// for auto-diff codegen...
		SCALAR zero = static_cast<SCALAR>(0.0);
		SCALAR g = static_cast<SCALAR>(9.81);
		gravityW_ << zero, zero, -g;
	}


	state_vector_euler_t state_; /*!< State */

	vector3_t gravityW_; /*!< Gravity expressed world frame */
};

typedef RBDStateEuler<12, double> RBDStateHyQ;

} //namespace

#endif /* INCLUDE_HYQ_GEN_AD_RBDSTATEEULER_HPP_ */
