/*
 * EEKinematics.h
 *
 *  Created on: Jul 19, 2016
 *      Author: neunertm
 */

#ifndef INCLUDE_HYQ_EEKINEMATICS_H_
#define INCLUDE_HYQ_EEKINEMATICS_H_

#include <cppad/cppad.hpp>
#include <cppad/example/cppad_eigen.hpp>

#include <HyQ/transforms.h>
#include <HyQ/jacobians.h>
#include <HyQ/link_data_map.h>

#include <iit/rbd/traits/DoubleTrait.h>

namespace hyq_gen_ad{

/**
 *  @brief Helper class to compute end-effector kinematics, i.e. EE positions and velocities
 */
template <typename TRAIT>
class EEKinematicsTpl
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    typedef typename TRAIT::Scalar SCALAR;

    typedef Eigen::Matrix<SCALAR, 3, 1> Vector3AD; /*!< Typedef for a 3D Vector */
    typedef Eigen::Matrix<SCALAR, 12, 1> JointAnglesAD; /*!< Typedef for 12D vector of joint angles */
    typedef RBDStateEuler<12, SCALAR> RBDStateHyQAD;

    typedef iit::HyQ::LinkDataMap<iit::rbd::ForceVectorTpl<SCALAR>> ExtForces; /*!< External forces exerted on the link expressed in link frame */

	/**
	 * @brief An enumeration for the leg ids
	 */
	/*! Leg IDs */
    enum LEG_ID {
            LF = 0, /*!< LF: 0*/
            RF = 1, /*!< RF: 1*/
            LH = 2, /*!< LH: 2*/
            RH = 3,  /*!< RH: 3*/
			N_LEGS = 4 /*< number of legs: 4 */
    };

    /**
     * Default Constructor
     * @param transforms RobCoGen Transformations (optional)
     * @param jacobians RobCoGen Jacobians (optional)
     */
    EEKinematicsTpl(
    		iit::HyQ::HomogeneousTransformsTpl<TRAIT>& transforms,
			iit::HyQ::JacobiansTpl<TRAIT>& jacobians) :
				transforms_(&transforms),
				jacobians_(&jacobians)
    {
    }

    /**
     * Get the end-effector (foot) position in base coordinates
     * @param B_eePos The position in base coordinates
     * @param LEG_ID The ID of the leg
     * @param jointAngles The current joint angles of the robot
     */
    void getEEPosInBase(
            Vector3AD& B_eePos,
            const LEG_ID& LEG_ID,
			const JointAnglesAD& jointAngles);

    /**
     * Get the end-effector (foot) position in world coordinates
     * @param W_eePos The position in world coordinates
     * @param LEG_ID The ID of the leg
     * @param RBDStateHyQAD The current state of the robot
     */
    void getEEPosInWorld(
            Vector3AD& W_eePos,
            const LEG_ID& LEG_ID,
			const RBDStateHyQAD& state);

    /**
     * Get the end-effector (foot) velocity in base coordinates
     * @param B_eeVel The velocity in base coordinates
     * @param LEG_ID LEG_ID The ID of the leg
     * @param RBDStateHyQAD The current state of the robot
     */
    void getEEVelInBase(
            Vector3AD& B_eeVel,
            const LEG_ID& LEG_ID,
			const RBDStateHyQAD& state);

    /**
     * Get the end-effector (foot) velocity in world coordinates
     * @param W_eeVel The velocity in world coordinates
     * @param LEG_ID LEG_ID The ID of the leg
     * @param RBDStateHyQAD The current state of the robot
     */
    void getEEVelInWorld(
            Vector3AD& W_eeVel,
            const LEG_ID& LEG_ID,
			const RBDStateHyQAD& state);


    /**
     * Converts forces exerted on the endeffectors to external forces on links
     * @param state The current state of the robot which also fully describes endeffetor positions in the world
     * @param eeForcesW Endeffector forces expressed in the world frame. They are assumed to be exerted on at the endeffector position.
     * @param extForces External forces in link frames as expected by RobCoGen
     *
     * RobCoGen expects contact/external forces to be expressed in the link frame
     * of the link they are exerted on. This function transforms endeffector forces
     * expressed in the world frame to the link frames of the links on which the
     * endeffector resides
     */
    void mapEEForcesToExtForces(
    		const RBDStateHyQAD& state,
    		const std::array<Vector3AD, N_LEGS>& eeForcesW,
			ExtForces& extForces
    );


private:
    iit::HyQ::HomogeneousTransformsTpl<TRAIT>* transforms_; /*!< Homogeneous Transforms (from RobCoGen) */
    iit::HyQ::JacobiansTpl<TRAIT>* jacobians_; /*!< Jacobians (from RobCoGen) */
};

using EEKinematics = EEKinematicsTpl<iit::rbd::DoubleTrait>;


#include "EEKinematics.impl.hpp"

} // hyq_gen_ad

#endif /* INCLUDE_HYQ_EEKINEMATICS_H_ */
