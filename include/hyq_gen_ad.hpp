/*! \file
 *	\brief		A general header to include all files in the correct order. Just include this file and you are good to go!
 *  \author	    Michael Neunert
 *
 */

#ifndef INCLUDE_HYQ_GEN_AD_HYQ_GEN_AD_HPP_
#define INCLUDE_HYQ_GEN_AD_HYQ_GEN_AD_HPP_

// This file is a general include for convenience
// It also ensures, includes are in the right order, which for CppAD is very important...
#include <iosfwd>
#include <vector>
#include <cppad/cg.hpp>

#include <cppad/cppad.hpp>
#include <cppad/example/cppad_eigen.hpp>
#include <cppad/example/eigen_mat_inv.hpp>

#include "hyq_gen_ad/RBDStateEuler.hpp"
#include "hyq_gen_ad/EEKinematics.hpp"

#include <HyQ/forward_dynamics.h>
#include <HyQ/inverse_dynamics.h>
#include <HyQ/transforms.h>
#include <HyQ/jsim.h>

#include <iit/rbd/traits/CppADCodegenTrait.h>
#include <iit/rbd/traits/CppADDoubleTrait.h>
#include <iit/rbd/traits/FloatTrait.h>

#endif /* INCLUDE_HYQ_GEN_AD_HYQ_GEN_AD_HPP_ */
