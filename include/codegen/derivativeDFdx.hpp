/*! \file
 *	\brief		Auto-generated code for computing the derivative of F with respect to x
 *  \author	    Michael Neunert
 *
 *  @example 	timingFullvsSeparateJacobian.cpp
 *  This is an example of how to use the generated code.
 *
 *  @example 	timingSparseJacobian.cpp
 *  This is an example of how to use the generated code.
 */

#ifndef TEST_CODEGEN_DERIVATIVEDFDX_HPP_
#define TEST_CODEGEN_DERIVATIVEDFDX_HPP_

#include <array>
#include <Eigen/Core>

namespace hyq_gen_ad {

Eigen::Matrix<double, 36, 18> computeDFdxCodegen(const Eigen::Matrix<double, 36, 1>& state, const Eigen::Matrix<double, 12, 1>& tau);

}

#endif /* TEST_CODEGEN_DERIVATIVEDFDX_HPP_ */
