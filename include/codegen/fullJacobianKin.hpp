/*! \file
 *	\brief		Auto-generated code for computing the derivative of qdd with respect to state and input
 *  \author	    Michael Neunert
 *
 *  @example 	timingFullvsSeparateJacobian.cpp
 *  This is an example of how to use the generated code.
 *
 *  @example 	timingFullJacobian.cpp
 *  This is an example of how to use the generated code.
 */

#ifndef TEST_CODEGEN_DERIVATIVE_FULL_JACOBIAN_KIN_HPP_
#define TEST_CODEGEN_DERIVATIVE_FULL_JACOBIAN_KIN_HPP_

#include <array>
#include <Eigen/Core>

namespace hyq_gen_ad {

Eigen::Matrix<double, 36, 4*6> computeFullJacobianKinCodegen(const Eigen::Matrix<double, 36, 1>& state);

}

#endif /* TEST_CODEGEN_DERIVATIVE_FULL_JACOBIAN_HPP_ */
