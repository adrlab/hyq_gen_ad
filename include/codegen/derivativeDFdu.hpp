/*! \file
 *	\brief		Auto-generated code for computing the derivative of F with respect to u
 *  \author	    Michael Neunert
 *
 *  @example 	timingFullvsSeparateJacobian.cpp
 *  This is an example of how to use the generated code.
 */


#ifndef TEST_CODEGEN_DERIVATIVEDFDU_HPP_
#define TEST_CODEGEN_DERIVATIVEDFDU_HPP_

#include <array>
#include <Eigen/Core>

namespace hyq_gen_ad {

Eigen::Matrix<double, 12, 18> computeDFduCodegen(const Eigen::Matrix<double, 36, 1>& state, const Eigen::Matrix<double, 12, 1>& tau);

}

#endif /* TEST_CODEGEN_DERIVATIVEDFDU_HPP_ */
