/*! \file 		sourceGeneration.cpp
 *	\brief		Example file of how to generate code from Auto-Diff
 *  \author	    Michael Neunert
 *
 *  @example 	sourceGeneration.cpp
 *  Example file of how to generate code from Auto-Diff.
 */

#define ROBCOGEN_AD_NAMESPACE CppAD

#include <hyq_gen_ad.hpp>

#include <ros/package.h>

using namespace CppAD;
using namespace CppAD::cg;
using namespace iit::rbd;
using namespace iit::HyQ;
using namespace hyq_gen_ad;


// use a special object for source code generation
typedef CG<double> CGD;
typedef AD<CGD> doubleAD;

/**
 * @brief Write source code files based on generated code using file templates
 * @param[in] f The auto-differentiated forward dynamics
 */
void writeCodeFile(const std::string& templateFileName, const std::string& outputFileName, const std::string& code);

/**
 * @brief Generates the derivative of joint accelerations with respect to joint torques
 * @param[in] f The auto-differentiated forward dynamics
 *
 * In the linearization f = Ax + Bu, where x is the full RBD state, this corresponds to the lower bottom
 * corner of B, e.g. in
 * B = df/du = [ dTrunkAcc/du dJointAcc/du ]
 * it corresponds to dJointAcc/du
 */
void generateDerivativeQddTau(CppAD::ADFun<CGD>& f);

/**
 * @brief Generates the derivative of the forward dynamics with respect to the RBD state
 * @param[in] f The auto-differentiated forward dynamics
 *
 * In a linearized model f = Ax + Bu, where x is the full RBD state, this computes the lower half of A.
 */
void generateA(CppAD::ADFun<CGD>& f);

/**
 * @brief Generates the derivative of the forward dynamics with respect to the input (joint torques)
 * @param[in] f The auto-differentiated forward dynamics
 *
 * In a linearized model f = Ax + Bu, where x is the full RBD state, this computes the lower half of B.
 * This is usually equivalent to M.inverse()*S
 */
void generateB(CppAD::ADFun<CGD>& f);

/**
 * @brief Generates the derivative of the forward dynamics with respect to both, the RBD state and tau
 * @param[in] f The auto-differentiated forward dynamics
 *
 * In a linearized model f = Ax + Bu, where x is the full RBD state, this computes both, the lower halves of A and B.
 */
void generateFullJacobian(CppAD::ADFun<CGD>& f);


/**
 * @brief Generates all derivatives
 */
int main(void) {


    typedef RBDStateEuler<12, doubleAD> RBDStateHyQAD;

    /***************************************************************************
     *                               the model
     **************************************************************************/

    // independent variable vector
    // in this test, we get the derivatives with respect to input

    std::cout << "Recording system dynamics... "<<std::endl;


	dyn::InertiaProperties inertiaProperties;
	MotionTransformsTpl<CppADCodegenTrait> motionTransforms;

	dyn::ForwardDynamicsTpl<CppADCodegenTrait> fd(inertiaProperties, motionTransforms);

	Eigen::Matrix<doubleAD, Eigen::Dynamic, 1> y(12+6); // CPPAD needs dynamic types...

	LinkDataMap<iit::rbd::ForceVectorTpl<doubleAD>> fext;
	for (size_t i=0; i<13; i++)
		fext[iit::HyQ::LinkIdentifiers(i)].setZero();


	// input vector
	Eigen::Matrix<doubleAD, Eigen::Dynamic, 1> x(36+12);

	// set random
	x.setRandom();

	// declare x as independent
	CppAD::Independent(x);

	// assign x to tau and state
	RBDStateHyQAD stateAD;
	stateAD.eigen() = x.topRows<36>();

	JointStateTpl<doubleAD> tauAD;
	tauAD = x.bottomRows<12>();


	JointStateTpl<doubleAD> qdd;
	VelocityVectorTpl<doubleAD> trunk_a;

	// call forward dynamics
	fd.fd(	qdd, trunk_a,
			stateAD.convertBaseVelocityBPluecker(),
			stateAD.computeGravityBPluecker(),
			stateAD.getJointPositions(),
			stateAD.getJointVelocities(),
			tauAD, fext);

	y << trunk_a, qdd; // CPPAD needs dynamic types...

	// store operation sequence in f: X -> Y and stop recording
	CppAD::ADFun<CGD> f(x, y);
	std::cout << "Recorded "<<f.size_op() << " operations." << std::endl;
	std::cout << "Trying to optimize... "<<std::endl;
	f.optimize();
	std::cout << "Optimized to "<<f.size_op() << " operations." << std::endl;

    /***************************************************************************
     *                        Generate the C source code
     **************************************************************************/

    /**
     * start the special steps for source code generation for a Jacobian
     */
	generateDerivativeQddTau(f);
	generateFullJacobian(f);
	generateA(f);
	generateB(f);
}


void generateDerivativeQddTau(CppAD::ADFun<CGD>& f)
{
	std::cout << std::endl << std::endl << "Starting codegeneration for qdd/qtau... "<<std::endl;

    CodeHandler<double> handler;

    CppAD::vector<CGD> input(36+12);
    handler.makeVariables(input);

	// the full derivative is a 38*18 Matrix:
	// dF/dx = [ A, B ]^T
	// where dim(A) = 36x18 = dim(state) dim(trunk_a, qdd)
	// and dim(B) = 12x18 = dim(tau) dim(trunk_a, qdd)
	CppAD::vector<bool> sparsity((36+12)*(12+6));
	Eigen::Map<Eigen::Matrix<bool, 36+12, 12+6>> sparsityEigen(sparsity.data());
	sparsityEigen.setZero();
	sparsityEigen.bottomRightCorner<12,12>().setOnes();

	//std::cout << "sparsity pattern of qdd/tau:" << std::endl << sparsityEigen << std::endl;

	size_t k = 12*12;
	CppAD::vector<size_t> row(k);
	CppAD::vector<size_t> col(k);
	for (size_t i=0; i<12; i++)
	{
		for(size_t j=0; j<12; j++)
		{
			row[i*12+j] = 6+i;
			col[i*12+j] = 36+j;
		}
	}

	CppAD::vector<CGD> jac(12*12);
	CppAD::sparse_jacobian_work work;

	std::cout << "Computing Jacobian... "<<std::endl;
	f.SparseJacobianForward(input, sparsity, row, col, jac, work);

	LanguageC<double> langC("double");
	LangCDefaultVariableNameGenerator<double> nameGen;

	std::ostringstream code;
	std::cout << "Generating code... "<<std::endl;
	handler.generateCode(code, langC, jac, nameGen);

	std::string templateFileName = "/templates/derivativeQddTau.cpp.template";
	std::string outputFileName = "/src/codegen/derivativeQddTau.cpp";
	writeCodeFile(templateFileName, outputFileName, code.str());
}


void generateA(CppAD::ADFun<CGD>& f)
{
	std::cout << std::endl << std::endl << "Starting code-generation for A, i.e. dF/dx... "<<std::endl;

    CodeHandler<double> handler;

    CppAD::vector<CGD> input(36+12);
    handler.makeVariables(input);

	// the full derivative is a 38*18 Matrix:
	// dF/dx = [ A, B ]^T
	// where dim(A) = 36x18 = dim(state) dim(trunk_a, qdd)
	// and dim(B) = 12x18 = dim(tau) dim(trunk_a, qdd)
	CppAD::vector<bool> sparsity((36+12)*(12+6));
	Eigen::Map<Eigen::Matrix<bool, 36+12, 12+6>> sparsityEigen(sparsity.data());
	sparsityEigen.setZero();
	sparsityEigen.topRows<36>().setOnes();

	//std::cout << "sparsity pattern of qdd/tau:" << std::endl << sparsityEigen << std::endl;

	size_t k = 36*18;
	CppAD::vector<size_t> row(k);
	CppAD::vector<size_t> col(k);
	for (size_t i=0; i<36; i++)
	{
		for(size_t j=0; j<18; j++)
		{
			row[i+j*36] = j;
			col[i+j*36] = i;
		}
	}

	CppAD::vector<CGD> jac(36*18);
	CppAD::sparse_jacobian_work work;

	std::cout << "Computing Jacobian... "<<std::endl;
	f.SparseJacobianForward(input, sparsity, row, col, jac, work);

	LanguageC<double> langC("double");
	LangCDefaultVariableNameGenerator<double> nameGen;

	std::ostringstream code;
	std::cout << "Generating code... "<<std::endl;
	handler.generateCode(code, langC, jac, nameGen);

	std::string templateFileName = "/templates/derivativeDFdx.cpp.template";
	std::string outputFileName = "/src/codegen/derivativeDFdx.cpp";
	writeCodeFile(templateFileName, outputFileName, code.str());
}

void generateB(CppAD::ADFun<CGD>& f)
{
	std::cout << std::endl << std::endl << "Starting codegeneration for B, i.e. dF/du... "<<std::endl;

    CodeHandler<double> handler;

    CppAD::vector<CGD> input(36+12);
    handler.makeVariables(input);

	// the full derivative is a 38*18 Matrix:
	// dF/dx = [ A, B ]^T
	// where dim(A) = 36x18 = dim(state) dim(trunk_a, qdd)
	// and dim(B) = 12x18 = dim(tau) dim(trunk_a, qdd)
	CppAD::vector<bool> sparsity((36+12)*(12+6));
	Eigen::Map<Eigen::Matrix<bool, 36+12, 12+6>> sparsityEigen(sparsity.data());
	sparsityEigen.setZero();
	sparsityEigen.bottomRows<12>().setOnes();

	//std::cout << "sparsity pattern of qdd/tau:" << std::endl << sparsityEigen << std::endl;

	size_t k = 12*18;
	CppAD::vector<size_t> row(k);
	CppAD::vector<size_t> col(k);
	for (size_t i=0; i<12; i++)
	{
		for(size_t j=0; j<18; j++)
		{
			row[i+j*12] = j;
			col[i+j*12] = i+36;
		}
	}

	CppAD::vector<CGD> jac(12*18);
	CppAD::sparse_jacobian_work work;

	std::cout << "Computing Jacobian... "<<std::endl;
	f.SparseJacobianForward(input, sparsity, row, col, jac, work);

	LanguageC<double> langC("double");
	LangCDefaultVariableNameGenerator<double> nameGen;

	std::ostringstream code;
	std::cout << "Generating code... "<<std::endl;
	handler.generateCode(code, langC, jac, nameGen);

	std::string templateFileName = "/templates/derivativeDFdu.cpp.template";
	std::string outputFileName = "/src/codegen/derivativeDFdu.cpp";
	writeCodeFile(templateFileName, outputFileName, code.str());
}


void generateFullJacobian(CppAD::ADFun<CGD>& f)
{
	std::cout << std::endl << std::endl << "Starting codegeneration of full Jacobian... "<<std::endl;

    CodeHandler<double> handler;

    CppAD::vector<CGD> input(36+12);
    handler.makeVariables(input);

	CppAD::vector<CGD> jac((36+12)*(6+12));

	std::cout << "Computing Jacobian... "<<std::endl;
	jac = f.Jacobian(input);

	LanguageC<double> langC("double");
	LangCDefaultVariableNameGenerator<double> nameGen;

	std::ostringstream code;
	std::cout << "Generating code... "<<std::endl;
	handler.generateCode(code, langC, jac, nameGen);

	std::string templateFileName = "/templates/fullJacobian.cpp.template";
	std::string outputFileName = "/src/codegen/fullJacobian.cpp";
	writeCodeFile(templateFileName, outputFileName, code.str());
}


void writeCodeFile(const std::string& templateFileName, const std::string& outputFileName, const std::string& code)
{
	std::string packageDir = ros::package::getPath("hyq_gen_ad");



	std::ifstream t(packageDir+templateFileName);

	if (!t.good())
	{
		std::cout << "Could not open template file "<<packageDir+templateFileName<<std::endl;
		std::cout << "Code generation failed. Exiting."<<std::endl;
		t.close();
		exit(-1);
	}

	std::stringstream templateFile;
	templateFile << t.rdbuf();

	std::string templateCode(templateFile.str());

	std::string placeholder = "AUTOGENERATED_CODE_PLACEHOLDER";
	templateCode.replace(templateCode.find(placeholder), placeholder.length(), code);

	t.close();

	std::ofstream out(packageDir+outputFileName);

	if (!out.good())
	{
		std::cout << "Could not open output file for writing "<<packageDir+outputFileName<<std::endl;
		std::cout << "Code generation failed. Exiting."<<std::endl;
		exit(-1);
	}

	std::cout << "Writing generated code to file... "<<std::endl;

	out << templateCode;
	out.close();

	std::cout << "Successfully generated derivative source code file: "<<packageDir+outputFileName<<std::endl;
}
