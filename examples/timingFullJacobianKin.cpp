/*! \file
 *	\brief		Timing comparison of full jacobian for endeffector kinematics
 *  \author	    Michael Neunert
 *
 *  @example 	timingFullJacobianKin.cpp
 *  Timing comparison of full Jacobian for endeffector kinematics
 *
 *  This file computes the full Jacobian of the endeffector kinematics
 *  (position, velocity) with respect to the state.
 *  It compares both timings and the actual values of the Jacobian
 *  using different ways of computing, i.e.
 *  - numerical differentiation
 *  - generated code from auto-diff
 */


#define ROBCOGEN_AD_NAMESPACE CppAD

#include <chrono>

#include <hyq_gen_ad.hpp>

#include <codegen/fullJacobianKin.hpp>

using namespace iit::rbd;
using namespace iit::HyQ;
using namespace hyq_gen_ad;

const size_t m = 36;
const size_t n = 4*6;

typedef Eigen::Matrix<double, m, n> Jacobian;

// NUM DIFF STUFF
double eps = 1e-6;//std::sqrt(Eigen::NumTraits<double>::epsilon());

HomogeneousTransforms transforms;
Jacobians jacobians;

hyq_gen_ad::EEKinematics eeKinematics(transforms, jacobians);
typedef hyq_gen_ad::EEKinematics::LEG_ID LEG_ID;

// in this test, we get the derivatives with respect to input
Jacobian computeNumDiff(
		const RBDStateHyQ& state)
{

	Jacobian jac;

	std::array<Eigen::Vector3d, 4> W_eePos_nominal;
	std::array<Eigen::Vector3d, 4> W_eeVel_nominal;

	for (size_t i=0; i<4; i++)
	{
		eeKinematics.getEEPosInWorld(W_eePos_nominal[i], LEG_ID(i), state);
		eeKinematics.getEEVelInWorld(W_eeVel_nominal[i], LEG_ID(i), state);
	}

	// input perturbation
	RBDStateHyQ state_perturbed;

	// output perturbation
	std::array<Eigen::Vector3d, 4> W_eePos_perturbed;
	std::array<Eigen::Vector3d, 4> W_eeVel_perturbed;


	for (size_t i=0; i<36; i++)
	{
		state_perturbed.eigen() = state.eigen();
		state_perturbed.eigen()(i) += eps;

		for (size_t j=0; j<4; j++)
		{
			eeKinematics.getEEPosInWorld(W_eePos_perturbed[j], LEG_ID(j), state_perturbed);
			eeKinematics.getEEVelInWorld(W_eeVel_perturbed[j], LEG_ID(j), state_perturbed);
		}

		jac.row(i) <<
				(W_eePos_perturbed[0] - W_eePos_nominal[0]).transpose()/eps,
				(W_eePos_perturbed[1] - W_eePos_nominal[1]).transpose()/eps,
				(W_eePos_perturbed[2] - W_eePos_nominal[2]).transpose()/eps,
				(W_eePos_perturbed[3] - W_eePos_nominal[3]).transpose()/eps,
				(W_eeVel_perturbed[0] - W_eeVel_nominal[0]).transpose()/eps,
				(W_eeVel_perturbed[1] - W_eeVel_nominal[1]).transpose()/eps,
				(W_eeVel_perturbed[2] - W_eeVel_nominal[2]).transpose()/eps,
				(W_eeVel_perturbed[3] - W_eeVel_nominal[3]).transpose()/eps;

	}

	return jac;
}




void speedTest()
{
	const size_t nTests = 10000;

	std::cout<<"Speedtest for full Jacobian of the derivative of end-effector kinematics with respect to state"<<std::endl;
	std::cout<<"=============================================================================================="<<std::endl;
	std::cout<<"Will now evaluate each method "<<nTests<<" times."<<std::endl<<std::endl;

	RBDStateHyQ state;
	state.eigen().setRandom();

	JointState qdd;
	qdd.setRandom();

	VelocityVector trunk_a;
	trunk_a.setRandom();


	std::vector<Jacobian> jacsNumDiff(nTests);
	std::vector<Jacobian> jacsCodegen(nTests);

	std::cout<<"Timing num diff... ";
	auto start = std::chrono::system_clock::now();
	for (size_t i=0; i<nTests; i++)
	{
		jacsNumDiff[i] = computeNumDiff(state);
	}
	auto end = std::chrono::system_clock::now();
	auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
	std::cout << "time num diff: "<< elapsed.count()*1000.0/nTests << " us/evaluation" << std::endl;

	std::cout<<"Timing auto-diff codegen... ";
	start = std::chrono::system_clock::now();
	for (size_t i=0; i<nTests; i++)
	{
		jacsCodegen[i] = computeFullJacobianKinCodegen(state.eigen());
	}
	end = std::chrono::system_clock::now();
	elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
	std::cout << "time auto-diff codegen: " << elapsed.count()*1000.0/nTests << " us/evaluation" << std::endl;



	for (size_t i=0; i<nTests; i++)
	{
		if (!jacsCodegen[i].isApprox(jacsNumDiff[i], 1e-6))
		{
			std::cout << "Jacobians at test " << i << " not equal!"<<std::endl;
			std::cout << "jac num diff: "<<std::endl<<jacsNumDiff[i]<<std::endl<<std::endl;
			std::cout << "jac codegen: "<<std::endl<<jacsCodegen[i]<<std::endl<<std::endl;
			std::cout << "diff: "<<std::endl<<jacsCodegen[i]-jacsNumDiff[i]<<std::endl<<std::endl;
			std::cout << std::endl<<std::endl;
			exit(-1);
		}
	}

	std::cout<<std::endl<<std::endl<<std::endl<<std::endl;
}


int main(int argc, char **argv){
	speedTest();
	return 0;
}
