/*! \file
 *	\brief		Timing comparison of full jacobian for inverse dynamcis
 *  \author	    Michael Neunert
 *
 *  @example 	timingFullJacobianId.cpp
 *  Timing comparison of full Jacobian for inverse dynamics
 *
 *  This file computes the full Jacobian of the fully actuated base
 *  inverse dynamics.
 *  It compares both timings and the actual values of the Jacobian
 *  using different ways of computing, i.e.
 *  - numerical differentiation
 *  - generated code from auto-diff
 */


#define ROBCOGEN_AD_NAMESPACE CppAD

#include <chrono>

#include <hyq_gen_ad.hpp>

#include <codegen/fullJacobianId.hpp>

using namespace iit::rbd;
using namespace iit::HyQ;
using namespace hyq_gen_ad;

const size_t m = 3*18;
const size_t n = 6+12;

typedef Eigen::Matrix<double, m, n> Jacobian;

// NUM DIFF STUFF
double eps = 1e-6;//std::sqrt(Eigen::NumTraits<double>::epsilon());

dyn::InertiaProperties inertiaProperties;
MotionTransforms motionTransforms;

dyn::InverseDynamics id(inertiaProperties, motionTransforms);

LinkDataMap<iit::rbd::ForceVector> fext;


// in this test, we get the derivatives with respect to input
Jacobian computeNumDiff(
		const RBDStateHyQ& state,
		const VelocityVector& trunk_a,
		const JointState& qdd)
{

	Jacobian jac;

	JointState jForces;
	VelocityVector baseWrench;

	id.id_fully_actuated(
			baseWrench, jForces,
			state.computeGravityBPluecker(),
			state.convertBaseVelocityBPluecker(),
			trunk_a,
			state.getJointPositions(),
			state.getJointVelocities(),
			qdd,
			fext);

	// input perturbation
	RBDStateHyQ state_perturbed;
	VelocityVector trunk_a_perturbed;
	JointState qdd_perturbed;

	// output perturbation
	VelocityVector baseWrench_perturbed;
	JointState jForces_perturbed;


	for (size_t i=0; i<36; i++)
	{
		state_perturbed.eigen() = state.eigen();
		state_perturbed.eigen()(i) += eps;

		id.id_fully_actuated(
				baseWrench_perturbed, jForces_perturbed,
				state_perturbed.computeGravityBPluecker(),
				state_perturbed.convertBaseVelocityBPluecker(),
				trunk_a,
				state_perturbed.getJointPositions(),
				state_perturbed.getJointVelocities(),
				qdd,
				fext);

		jac.row(i) << (baseWrench_perturbed - baseWrench).transpose()/eps, (jForces_perturbed-jForces).transpose()/eps;

	}

	for (size_t i=0; i<6; i++)
	{
		trunk_a_perturbed = trunk_a;
		trunk_a_perturbed(i) += eps;

		id.id_fully_actuated(
				baseWrench_perturbed, jForces_perturbed,
				state.computeGravityBPluecker(),
				state.convertBaseVelocityBPluecker(),
				trunk_a_perturbed,
				state.getJointPositions(),
				state.getJointVelocities(),
				qdd,
				fext);

		jac.row(36+i) << (baseWrench_perturbed - baseWrench).transpose()/eps, (jForces_perturbed-jForces).transpose()/eps;
	}

	for (size_t i=0; i<12; i++)
	{
		qdd_perturbed = qdd;
		qdd_perturbed(i) += eps;

		id.id_fully_actuated(
				baseWrench_perturbed, jForces_perturbed,
				state.computeGravityBPluecker(),
				state.convertBaseVelocityBPluecker(),
				trunk_a,
				state.getJointPositions(),
				state.getJointVelocities(),
				qdd_perturbed,
				fext);

		jac.row(36+6+i) << (baseWrench_perturbed - baseWrench).transpose()/eps, (jForces_perturbed-jForces).transpose()/eps;
	}

	return jac;
}




void speedTest()
{
	const size_t nTests = 10000;

	std::cout<<"Speedtest for full Jacobian of the derivative of the Inverse Dynamics with respect to states and desired accelerations"<<std::endl;
	std::cout<<"======================================================================================================================"<<std::endl;
	std::cout<<"Will now evaluate each method "<<nTests<<" times."<<std::endl<<std::endl;

	RBDStateHyQ state;
	state.eigen().setRandom();

	JointState qdd;
	qdd.setRandom();

	VelocityVector trunk_a;
	trunk_a.setRandom();


	// NUMDIFF STUFF
	for (size_t i=0; i<13; i++)
		fext[iit::HyQ::LinkIdentifiers(i)].setZero();


	std::vector<Jacobian> jacsNumDiff(nTests);
	std::vector<Jacobian> jacsCodegen(nTests);

	std::cout<<"Timing num diff... ";
	auto start = std::chrono::system_clock::now();
	for (size_t i=0; i<nTests; i++)
	{
		jacsNumDiff[i] = computeNumDiff(state, trunk_a, qdd);
	}
	auto end = std::chrono::system_clock::now();
	auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
	std::cout << "time num diff: "<< elapsed.count()*1000.0/nTests << " us/evaluation" << std::endl;

	std::cout<<"Timing auto-diff codegen... ";
	start = std::chrono::system_clock::now();
	for (size_t i=0; i<nTests; i++)
	{
		jacsCodegen[i] = computeFullJacobianIdCodegen(state.eigen(), trunk_a, qdd);
	}
	end = std::chrono::system_clock::now();
	elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
	std::cout << "time auto-diff codegen: " << elapsed.count()*1000.0/nTests << " us/evaluation" << std::endl;



	for (size_t i=0; i<nTests; i++)
	{
		if (!jacsCodegen[i].isApprox(jacsNumDiff[i], 1e-6))
		{
			std::cout << "Jacobians at test " << i << " not equal!"<<std::endl;
			std::cout << "jac num diff: "<<std::endl<<jacsNumDiff[i]<<std::endl<<std::endl;
			std::cout << "jac codegen: "<<std::endl<<jacsCodegen[i]<<std::endl<<std::endl;
			std::cout << "diff: "<<std::endl<<jacsCodegen[i]-jacsNumDiff[i]<<std::endl<<std::endl;
			std::cout << std::endl<<std::endl;
			exit(-1);
		}
	}

	std::cout<<std::endl<<std::endl<<std::endl<<std::endl;
}


int main(int argc, char **argv){
	speedTest();
	return 0;
}
