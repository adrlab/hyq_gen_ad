/*! \file
 *	\brief		Timing comparison of full jacobian computed in two separate steps
 *  \author	    Michael Neunert
 *
 *  @example 	timingFullvsSeparateJacobian.cpp
 *  Timing comparison of full jacobian computed in two separate steps
 *
 *  This file computes the full Jacobian, i.e. the lower halves of A and B
 *  in f = Ax + Bu, where x is the full RBD state.
 *  It compares both timings and the actual values of the Jacobian
 *  using different ways of computing, i.e.
 *  - numerical differentiation
 *  - separate generated code for A and B obtained from auto-diff
 */


#define ROBCOGEN_AD_NAMESPACE CppAD

#include <chrono>

#include <hyq_gen_ad.hpp>

#include <codegen/fullJacobian.hpp>
#include <codegen/derivativeDFdu.hpp>
#include <codegen/derivativeDFdx.hpp>

using namespace iit::rbd;
using namespace iit::HyQ;
using namespace hyq_gen_ad;

const size_t m = 36+12;
const size_t n = 18;

typedef Eigen::Matrix<double, m, n> Jacobian;


Jacobian computeSeparateJacCodegen(
		const RBDStateHyQ& state,
		const JointState& tau)
{
	Jacobian jac;

	jac.topRows<36>() = computeDFdxCodegen(state.eigen(), tau);

	jac.bottomRows<12>() = computeDFduCodegen(state.eigen(), tau);

	return jac;
}



void speedTest()
{
	const size_t nTests = 10000;

	std::cout<<"Speedtest for separate vs full Jacobian dF/dx, both from AD generated C++ code."<<std::endl;
	std::cout<<"==============================================================================="<<std::endl;
	std::cout<<"Will now evaluate each method "<<nTests<<" times."<<std::endl<<std::endl;

	RBDStateHyQ state;
	state.eigen().setRandom();

	JointState tau;
	tau.setRandom();

	std::vector<Jacobian> jacsSeparateCodegen(nTests);
	std::vector<Jacobian> jacsFullCodegen(nTests);

	std::cout<<"Timing separate codegen... ";
	auto start = std::chrono::system_clock::now();
	for (size_t i=0; i<nTests; i++)
	{
		jacsSeparateCodegen[i] = computeSeparateJacCodegen(state, tau);
	}
	auto end = std::chrono::system_clock::now();
	auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
	std::cout << "time separate codegen: "<< elapsed.count()*1000.0/nTests << " us/evaluation" << std::endl;


	std::cout<<"Timing full codegen... ";
	start = std::chrono::system_clock::now();
	for (size_t i=0; i<nTests; i++)
	{
		jacsFullCodegen[i] = computeFullJacobianCodegen(state.eigen(), tau);
	}
	end = std::chrono::system_clock::now();
	elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
	std::cout << "time full codegen: " <<elapsed.count()*1000.0/nTests << " us/evaluation" << std::endl;

	for (size_t i=0; i<nTests; i++)
	{
		if (!jacsSeparateCodegen[i].isApprox(jacsFullCodegen[i], 1e-6))
		{
			std::cout << "Jacobians at test " << i << " not equal!"<<std::endl;
			std::cout << "jac separate codegen: "<<std::endl<<jacsSeparateCodegen[i]<<std::endl<<std::endl;
			std::cout << "jac full codegen: "<<std::endl<<jacsFullCodegen[i]<<std::endl<<std::endl;
			std::cout << "diff: "<<std::endl<<jacsSeparateCodegen[i]-jacsFullCodegen[i]<<std::endl<<std::endl;
			std::cout << std::endl<<std::endl;
			exit(-1);
		}
	}

	std::cout<<std::endl<<std::endl<<std::endl<<std::endl;
}


int main(int argc, char **argv){
	speedTest();
	return 0;
}
