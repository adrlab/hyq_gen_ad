/*! \file
 *	\brief		Timing of the Jacobian of the joint acceleration with respect to tau
 *  \author	    Michael Neunert
 *
 *  @example 	timingDerivativeQddTau.cpp
 *  Example file of how to use the generated derivatives
 *
 *  This file computes the Jacobian of the joint acceleration with respect to tau
 *  the bottom right corner of B
 *  in f = Ax + Bu, where x is the full RBD state. The analytical solution is
 *  J = M.inverse()*S, where S is the selection matrix
 *  It compares both timings and the actual values of the Jacobian
 *  using different ways of computing, i.e.
 *  - numerical differentiation
 *  - auto-diff (using CppAD)
 *  - generated code from auto-diff (using CppAD Codegen)
 *  - analytic derivative using LLT factorization (by Eigen)
 *  - analytic derivative using Partial Pivoting LU factorization (by Eigen)
 */


#define ROBCOGEN_AD_NAMESPACE CppAD

#include <chrono>

#include <hyq_gen_ad.hpp>

#include <codegen/derivativeQddTau.hpp>

using namespace iit::rbd;
using namespace iit::HyQ;
using namespace hyq_gen_ad;

const size_t m = 12;
const size_t n = 12;

typedef Eigen::Matrix<double, m, n> Jacobian;



// NUM DIFF STUFF
double eps = std::sqrt(Eigen::NumTraits<double>::epsilon());

dyn::InertiaProperties inertiaProperties;
MotionTransforms motionTransforms;

dyn::ForwardDynamics fd(inertiaProperties, motionTransforms);

LinkDataMap<iit::rbd::ForceVector> fext;



// AD STUFF
typedef CppAD::AD<double> doubleAD;
MotionTransformsTpl<CppADDoubleTrait> motionTransformsAD;

dyn::ForwardDynamicsTpl<CppADDoubleTrait> fdAD(inertiaProperties, motionTransformsAD);

LinkDataMap<iit::rbd::ForceVectorTpl<doubleAD>> fextAD;

CppAD::ADFun<double> f;
std::vector<double> tau_lin(n);



// Analytic stuff
iit::HyQ::dyn::InertiaProperties ip;
iit::HyQ::ForceTransforms ft;
iit::HyQ::dyn::JSIM jsim(ip, ft);
typedef Eigen::Matrix<double, 18, 18> jsim_t;
Eigen::Matrix<double, 18, 12> S; // selection matrix
Eigen::LLT<jsim_t> llt;


// in this test, we get the derivatives with respect to input
Jacobian computeNumDiff(
		const RBDStateHyQ& state,
		const JointState& tau)
{

	Jacobian jac;

	JointState qddNominal;
	VelocityVector trunk_a;

	fd.fd(	qddNominal, trunk_a,
			state.convertBaseVelocityBPluecker(),
			state.computeGravityBPluecker(),
			state.getJointPositions(),
			state.getJointVelocities(),
			tau, fext);

	JointState tauPerturbed;
	JointState qddPerturbed;

	for (size_t i=0; i<m; i++)
	{
		tauPerturbed = tau;
		tauPerturbed(i) += eps;

		fd.fd(	qddPerturbed, trunk_a,
				state.convertBaseVelocityBPluecker(),
				state.computeGravityBPluecker(),
				state.getJointPositions(),
				state.getJointVelocities(),
				tauPerturbed, fext);

		jac.col(i) = (qddPerturbed - qddNominal)/eps;
	}

	return jac;
}

void prepareAD(
		const RBDStateEuler<12, doubleAD>& stateAD,
		JointStateTpl<doubleAD>& tauAD)
{
	Eigen::Matrix<doubleAD, Eigen::Dynamic, 1> qddDyn(12);
	VelocityVectorTpl<doubleAD> trunk_a;

	Eigen::Matrix<doubleAD, Eigen::Dynamic, 1> tauDyn(12);
	tauDyn << tauAD;

	// declare tau as independent
	CppAD::Independent(tauDyn);

	tauAD = tauDyn;

	JointStateTpl<doubleAD> qdd;

	// call forward dynamics
	fdAD.fd(	qdd, trunk_a,
				stateAD.convertBaseVelocityBPluecker(),
				stateAD.computeGravityBPluecker(),
				stateAD.getJointPositions(),
				stateAD.getJointVelocities(),
				tauAD, fextAD);

	qddDyn = qdd;

	// store operation sequence in f: X -> Y and stop recording
	CppAD::ADFun<double> f_local(tauDyn, qddDyn);
	f_local.optimize();

	f = f_local;
}

void prepareAnalytic()
{
	S.setZero();
	S.block<12,12>(6,0) = Eigen::Matrix<double, 12, 12>::Identity();
}

// in this test, we get the derivatives with respect to input
__attribute__((noinline)) Jacobian computeAD()
{
	// compute jacobian
	// TODO: Make these directly eigen types
	std::vector<double> jac(m * n); 	// Jacobian of f (m by n matrix)

	jac  = f.Jacobian(tau_lin);      	// Jacobian for operation sequence

	// convert to Eigen
	Eigen::Map<Eigen::Matrix<double, m, n>> jacEigen(jac.data());

	// return the results
	return jacEigen;
}

// This uses the efficient inverse of RobCoGen to compute the derivative
__attribute__((noinline)) Eigen::Matrix<double, m, n> computeRobCoGenLLT(
    const RBDStateHyQ& state,
    const JointState& tau)
{
    // There is a bug in Robcogen that renders this method incorrect for floating base systems. Still, we want to time it!

    const jsim_t& M = jsim.update(state.getJointPositions());

    jsim.computeL();
    jsim.computeInverse();

    return jsim.getInverse().bottomRightCorner<12,12>();
}

__attribute__((noinline)) Eigen::Matrix<double, m, n> computeAnalyticLLT(
	const RBDStateHyQ& state,
	const JointState& tau)
{
	const jsim_t& M = jsim.update(state.getJointPositions());
	llt.compute(M);

	const jsim_t& M_inv = llt.solve(jsim_t::Identity());

	auto dFdu = M_inv * S;

	return dFdu.bottomRows<12>();
}

__attribute__((noinline)) Eigen::Matrix<double, m, n> computeAnalyticPartialPivLu(
	const RBDStateHyQ& state,
	const JointState& tau)
{
	const jsim_t& M = jsim.update(state.getJointPositions());

	auto dFdu = M.partialPivLu().inverse() * S;

	return dFdu.bottomRows<12>();
}

void speedTest(bool fastestOnly)
{
	const size_t nTests = 10000;

	std::cout<<"Speedtest for Jacobian d_qdd / d_tau, i.e. the derivative of joint acceleration with respect to joint torques"<<std::endl;
	std::cout<<"============================================================================================================="<<std::endl;
	std::cout<<"Analytically, this is M^(-1) * S, where M is mass matrix and S is selection matrix."<<std::endl;
	std::cout<<"Will now evaluate each method "<<nTests<<" times."<<std::endl<<std::endl;

	RBDStateHyQ state;
	state.eigen().setRandom();

	JointState tau;
	tau.setRandom();


	// NUMDIFF STUFF
	for (size_t i=0; i<13; i++)
		fext[iit::HyQ::LinkIdentifiers(i)].setZero();


	// AUTODIFF STUFF
	RBDStateEuler<12, doubleAD> stateAD;
	stateAD.eigen() = state.eigen().cast<doubleAD>();

	JointStateTpl<doubleAD> tauAD = tau.cast<doubleAD>();

	for (size_t i=0; i<13; i++)
		fextAD[iit::HyQ::LinkIdentifiers(i)].setZero();
	for (size_t i=0; i<n; i++)
		tau_lin[i] = tau(i);

	prepareAD(stateAD, tauAD);
	prepareAnalytic();

	std::vector<Jacobian> jacsNumDiff(nTests);
	std::vector<Jacobian> jacsAD(nTests);
	std::vector<Jacobian> jacsCodegen(nTests);
	std::vector<Jacobian> jacsAnalyticLLT(nTests);
	std::vector<Jacobian> jacsAnalyticPartialPiv(nTests);
	std::vector<Jacobian> jacsRobCoGenLLT(nTests);

	std::cout<<"Timing auto-diff codegen... ";
	auto start = std::chrono::system_clock::now();
	for (size_t i=0; i<nTests; i++)
	{
		jacsCodegen[i] = computeQddTauCodegen(state.eigen(), tau);
	}
	auto end = std::chrono::system_clock::now();
	auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
	std::cout << elapsed.count()*1000.0/nTests << " us/evaluation" << std::endl;

    std::cout<<"Timing RobCoGen LLT... ";
    start = std::chrono::system_clock::now();
    for (size_t i=0; i<nTests; i++)
    {
        jacsRobCoGenLLT[i] = computeRobCoGenLLT(state, tau);
    }
    end = std::chrono::system_clock::now();
    elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    std::cout << elapsed.count()*1000.0/nTests << " us/evaluation" << std::endl;

	if (!fastestOnly)
	{
		std::cout<<"Timing num diff... ";
		start = std::chrono::system_clock::now();
		for (size_t i=0; i<nTests; i++)
		{
			jacsNumDiff[i] = computeNumDiff(state, tau);
		}
		end = std::chrono::system_clock::now();
		elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
		std::cout << "time num diff: "<< elapsed.count()*1000.0/nTests << " us/evaluation" << std::endl;

		std::cout<<"Timing auto diff... ";
		start = std::chrono::system_clock::now();
		for (size_t i=0; i<nTests; i++)
		{
			jacsAD[i] = computeAD();
		}
		end = std::chrono::system_clock::now();
		elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
		std::cout << elapsed.count()*1000.0/nTests << " us/evaluation" << std::endl;


		std::cout<<"Timing analytic LLT... ";
		start = std::chrono::system_clock::now();
		for (size_t i=0; i<nTests; i++)
		{
			jacsAnalyticLLT[i] = computeAnalyticLLT(state, tau);
		}
		end = std::chrono::system_clock::now();
		elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
		std::cout << elapsed.count()*1000.0/nTests << " us/evaluation" << std::endl;

		std::cout<<"Timing analytic PartialPivLU... ";
		start = std::chrono::system_clock::now();
		for (size_t i=0; i<nTests; i++)
		{
			jacsAnalyticPartialPiv[i] = computeAnalyticPartialPivLu(state, tau);
		}
		end = std::chrono::system_clock::now();
		elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
		std::cout << elapsed.count()*1000.0/nTests << " us/evaluation" << std::endl;




		for (size_t i=0; i<nTests; i++)
		{
			// we are not testing the robcogen LLT since it is wrong for floating base systems

			if (!jacsAD[i].isApprox(jacsNumDiff[i], 1e-6))
			{
				std::cout << "Jacobians at test " << i << " not equal!"<<std::endl;
				std::cout << "jac num diff: "<<std::endl<<jacsNumDiff[i]<<std::endl<<std::endl;
				std::cout << "jac AD: "<<std::endl<<jacsAD[i]<<std::endl<<std::endl;
				std::cout << "diff: "<<std::endl<<jacsAD[i]-jacsNumDiff[i]<<std::endl<<std::endl;
				std::cout << std::endl<<std::endl;
				exit(-1);
			}

			if (!jacsAD[i].isApprox(jacsCodegen[i], 1e-12))
			{
				std::cout << "Jacobians at test " << i << " not equal!"<<std::endl;
				std::cout << "jac num diff: "<<std::endl<<jacsCodegen[i]<<std::endl<<std::endl;
				std::cout << "jac AD: "<<std::endl<<jacsAD[i]<<std::endl<<std::endl;
				std::cout << "diff: "<<std::endl<<jacsAD[i]-jacsCodegen[i]<<std::endl<<std::endl;
				std::cout << std::endl<<std::endl;
				exit(-1);
			}

			if (!jacsAD[i].isApprox(jacsAnalyticLLT[i], 1e-12))
			{
				std::cout << "Jacobians at test " << i << " not equal!"<<std::endl;
				std::cout << "jac analytic: "<<std::endl<<jacsAnalyticLLT[i]<<std::endl<<std::endl;
				std::cout << "jac AD: "<<std::endl<<jacsAD[i]<<std::endl<<std::endl;
				std::cout << "diff: "<<std::endl<<jacsAD[i]-jacsAnalyticLLT[i]<<std::endl<<std::endl;
				std::cout << std::endl<<std::endl;
				exit(-1);
			}

			if (!jacsAD[i].isApprox(jacsAnalyticPartialPiv[i], 1e-12))
			{
				std::cout << "Jacobians at test " << i << " not equal!"<<std::endl;
				std::cout << "jac analytic: "<<std::endl<<jacsAnalyticPartialPiv[i]<<std::endl<<std::endl;
				std::cout << "jac AD: "<<std::endl<<jacsAD[i]<<std::endl<<std::endl;
				std::cout << "diff: "<<std::endl<<jacsAD[i]-jacsAnalyticPartialPiv[i]<<std::endl<<std::endl;
				std::cout << std::endl<<std::endl;
				exit(-1);
			}
		}
	}

	std::cout<<std::endl<<std::endl<<std::endl<<std::endl;
}


int main(int argc, char **argv){
	bool fastestOnly = (argc>1);
	speedTest(fastestOnly);
	return 0;
}
