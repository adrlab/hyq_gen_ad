/*! \file
 *	\brief		Timing comparison of full jacobian
 *  \author	    Michael Neunert
 *
 *  @example 	timingFullJacobian.cpp
 *  Timing comparison of full Jacobian
 *
 *  This file computes the full Jacobian, i.e. the lower halves of A and B
 *  in f = Ax + Bu, where x is the full RBD state.
 *  It compares both timings and the actual values of the Jacobian
 *  using different ways of computing, i.e.
 *  - numerical differentiation
 *  - generated code from auto-diff
 */


#define ROBCOGEN_AD_NAMESPACE CppAD

#include <chrono>

#include <hyq_gen_ad.hpp>

#include <codegen/fullJacobian.hpp>

using namespace iit::rbd;
using namespace iit::HyQ;
using namespace hyq_gen_ad;

const size_t m = 36+12;
const size_t n = 6+12;

typedef Eigen::Matrix<double, m, n> Jacobian;

// NUM DIFF STUFF
double eps = 1e-6;//std::sqrt(Eigen::NumTraits<double>::epsilon());

dyn::InertiaProperties inertiaProperties;
MotionTransforms motionTransforms;

dyn::ForwardDynamics fd(inertiaProperties, motionTransforms);

LinkDataMap<iit::rbd::ForceVector> fext;


// in this test, we get the derivatives with respect to input
Jacobian computeNumDiff(
		const RBDStateHyQ& state,
		const JointState& tau)
{

	Jacobian jac;

	JointState qdd_nominal;
	VelocityVector trunk_a_nominal;

	fd.fd(	qdd_nominal, trunk_a_nominal,
			state.convertBaseVelocityBPluecker(),
			state.computeGravityBPluecker(),
			state.getJointPositions(),
			state.getJointVelocities(),
			tau, fext);

	// input perturbation
	RBDStateHyQ state_perturbed;
	JointState tau_perturbed;

	// output perturbation
	JointState qdd_perturbed;
	VelocityVector trunk_a_perturbed;


	for (size_t i=0; i<36; i++)
	{
		state_perturbed.eigen() = state.eigen();
		state_perturbed.eigen()(i) += eps;

		fd.fd(	qdd_perturbed, trunk_a_perturbed,
				state_perturbed.convertBaseVelocityBPluecker(),
				state_perturbed.computeGravityBPluecker(),
				state_perturbed.getJointPositions(),
				state_perturbed.getJointVelocities(),
				tau, fext);

		jac.row(i) << (trunk_a_perturbed - trunk_a_nominal).transpose()/eps, (qdd_perturbed-qdd_nominal).transpose()/eps;

	}

	for (size_t i=0; i<12; i++)
	{
		tau_perturbed = tau;
		tau_perturbed(i) += eps;

		fd.fd(	qdd_perturbed, trunk_a_perturbed,
				state.convertBaseVelocityBPluecker(),
				state.computeGravityBPluecker(),
				state.getJointPositions(),
				state.getJointVelocities(),
				tau_perturbed, fext);

		jac.row(36+i) << (trunk_a_perturbed - trunk_a_nominal).transpose()/eps, (qdd_perturbed-qdd_nominal).transpose()/eps;
	}

	return jac;
}




void speedTest()
{
	const size_t nTests = 10000;

	std::cout<<"Speedtest for full Jacobian d_qdd / d_x, i.e. the derivative of base and joint acceleration with respect to states and torques"<<std::endl;
	std::cout<<"=============================================================================================================================="<<std::endl;
	std::cout<<"Will now evaluate each method "<<nTests<<" times."<<std::endl<<std::endl;

	RBDStateHyQ state;
	state.eigen().setRandom();

	JointState tau;
	tau.setRandom();


	// NUMDIFF STUFF
	for (size_t i=0; i<13; i++)
		fext[iit::HyQ::LinkIdentifiers(i)].setZero();


	std::vector<Jacobian> jacsNumDiff(nTests);
	std::vector<Jacobian> jacsCodegen(nTests);

	std::cout<<"Timing num diff... ";
	auto start = std::chrono::system_clock::now();
	for (size_t i=0; i<nTests; i++)
	{
		jacsNumDiff[i] = computeNumDiff(state, tau);
	}
	auto end = std::chrono::system_clock::now();
	auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
	std::cout << "time num diff: "<< elapsed.count()*1000.0/nTests << " us/evaluation" << std::endl;

	std::cout<<"Timing auto-diff codegen... ";
	start = std::chrono::system_clock::now();
	for (size_t i=0; i<nTests; i++)
	{
		jacsCodegen[i] = computeFullJacobianCodegen(state.eigen(), tau);
	}
	end = std::chrono::system_clock::now();
	elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
	std::cout << "time auto-diff codegen: " << elapsed.count()*1000.0/nTests << " us/evaluation" << std::endl;



	for (size_t i=0; i<nTests; i++)
	{
		if (!jacsCodegen[i].isApprox(jacsNumDiff[i], 1e-6))
		{
			std::cout << "Jacobians at test " << i << " not equal!"<<std::endl;
			std::cout << "jac num diff: "<<std::endl<<jacsNumDiff[i]<<std::endl<<std::endl;
			std::cout << "jac codegen: "<<std::endl<<jacsCodegen[i]<<std::endl<<std::endl;
			std::cout << "diff: "<<std::endl<<jacsCodegen[i]-jacsNumDiff[i]<<std::endl<<std::endl;
			std::cout << std::endl<<std::endl;
			exit(-1);
		}
	}

	std::cout<<std::endl<<std::endl<<std::endl<<std::endl;
}


int main(int argc, char **argv){
	speedTest();
	return 0;
}
