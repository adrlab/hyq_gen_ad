/*
 * testDerivatives.cpp
 *
 *  Created on: Aug 9, 2016
 *      Author: neunertm
 */

#define ROBCOGEN_AD_NAMESPACE CppAD

#include <gtest/gtest.h>

#include <hyq_gen_ad.hpp>

#include <codegen/derivativeQddTau.hpp>

using namespace iit::rbd;
using namespace iit::HyQ;
using namespace hyq_gen_ad;

const size_t m = 12;
const size_t n = 12;

typedef RBDStateEuler<12, CppAD::AD<double> > RBDStateHyQAD;


// in this test, we get the derivatives with respect to input
Eigen::Matrix<double, m, n> computeNumDiff(
		const RBDStateHyQ& state,
		const JointState& tau)
{
	double eps = std::sqrt(Eigen::NumTraits<double>::epsilon());

	dyn::InertiaProperties inertiaProperties;
	MotionTransforms motionTransforms;

	dyn::ForwardDynamics fd(inertiaProperties, motionTransforms);

	LinkDataMap<iit::rbd::ForceVector> fext;
	for (size_t i=0; i<13; i++)
		fext[iit::HyQ::LinkIdentifiers(i)].setZero();

	Eigen::Matrix<double, m, n> jac;

	JointState qddNominal;
	VelocityVector trunk_a;

	fd.fd(	qddNominal, trunk_a,
			state.convertBaseVelocityBPluecker(),
			state.computeGravityBPluecker(),
			state.getJointPositions(),
			state.getJointVelocities(),
			tau, fext);

	JointState tauPerturbed;
	JointState qddPerturbed;

	for (size_t i=0; i<m; i++)
	{
		tauPerturbed = tau;
		tauPerturbed(i) += eps;

		fd.fd(	qddPerturbed, trunk_a,
				state.convertBaseVelocityBPluecker(),
				state.computeGravityBPluecker(),
				state.getJointPositions(),
				state.getJointVelocities(),
				tauPerturbed, fext);

		jac.col(i) = (qddPerturbed - qddNominal)/eps;
	}

	return jac;
}

// in this test, we get the derivatives with respect to input
Eigen::Matrix<double, m, n> computeAD(
		const RBDStateHyQ& state,
		const JointState& tau)
{
	// setup the problem
	typedef CppAD::AD<double> doubleAD;

	dyn::InertiaProperties inertiaProperties;
	MotionTransformsTpl<CppADDoubleTrait> motionTransforms;

	dyn::ForwardDynamicsTpl<CppADDoubleTrait> fd(inertiaProperties, motionTransforms);

	Eigen::Matrix<doubleAD, Eigen::Dynamic, 1> qdd_dynamic(12); // CPPAD needs dynamic types...
	VelocityVectorTpl<doubleAD> trunk_a;

	RBDStateHyQAD stateAD;
	stateAD.eigen() = state.eigen().cast<doubleAD>();

	Eigen::Matrix<doubleAD, Eigen::Dynamic, 1> tauAD_dynamic = tau.cast<doubleAD>(); // CPPAD needs dynamic types...
	LinkDataMap<iit::rbd::ForceVectorTpl<doubleAD>> fext;
	for (size_t i=0; i<13; i++)
		fext[iit::HyQ::LinkIdentifiers(i)].setZero();

	// declare tau as independent
	CppAD::Independent(tauAD_dynamic);
	const size_t n = JointState::RowsAtCompileTime;

	JointStateTpl<doubleAD> tauAD = tauAD_dynamic; // CPPAD needs dynamic types...
	JointStateTpl<doubleAD> qdd; // CPPAD needs dynamic types...

	// call forward dynamics
	fd.fd(	qdd, trunk_a,
			stateAD.convertBaseVelocityBPluecker(),
			stateAD.computeGravityBPluecker(),
			stateAD.getJointPositions(),
			stateAD.getJointVelocities(),
			tauAD, fext);

	const size_t m = JointState::RowsAtCompileTime;

	qdd_dynamic = qdd; // CPPAD needs dynamic types...

	// store operation sequence in f: X -> Y and stop recording
	CppAD::ADFun<double> f(tauAD_dynamic, qdd_dynamic);

	// compute jacobian
	// TODO: Make these directly eigen types
	std::vector<double> jac(m * n); 	// Jacobian of f (m by n matrix)
	std::vector<double> tau_lin(n);
//	Eigen::Matrix<double, m, n> jac;
//	Eigen::Matrix<double, n, 1> tau_lin;

	for (size_t i=0; i<n; i++)
		tau_lin[i] = 0.0;
	jac  = f.Jacobian(tau_lin);      	// Jacobian for operation sequence

	// convert to Eigen
	Eigen::Map<Eigen::Matrix<double, m, n>> jacEigen(jac.data());

	// return the results
	return jacEigen;
}

Eigen::Matrix<double, m, n> computeAnalytic(
	const RBDStateHyQ& state,
	const JointState& tau)
{
	typedef Eigen::Matrix<double, 18, 18> jsim_t;

	iit::HyQ::dyn::InertiaProperties ip;
	iit::HyQ::ForceTransforms ft;
	iit::HyQ::dyn::JSIM jsim(ip, ft);


	// Set up selection matrix
	Eigen::Matrix<double, 18, 12> S;
	S.setZero();
	S.block<12,12>(6,0) = Eigen::Matrix<double, 12, 12>::Identity();

	Eigen::LLT<jsim_t> llt;

	const jsim_t& M = jsim.update(state.getJointPositions());
	llt.compute(M);

	const jsim_t& M_inv = llt.solve(jsim_t::Identity());

	if (!M.inverse().isApprox(M_inv))
	{
		std::cout << "M inverse incorrect" <<std::endl;
		std::cout << "M.inverse: " <<std::endl<<M.inverse()<<std::endl;
		std::cout << "M_inv: " <<std::endl<<M_inv<<std::endl;
//		ASSERT_TRUE(false);
	}

	auto dFdu = M_inv * S;

	return dFdu.bottomRows<12>();
}

TEST(derivativeTest, torqueJacobianTest)
{
	RBDStateHyQ state;
	JointState tau;

	const size_t nTests = 100;

	double precisionAD = 0;
	double precisionNumDiff = 0;
	double precisionCodegen = 0;

	for (size_t i=0; i<nTests; i++)
	{
		state.eigen().setRandom();
		tau.setRandom();

		auto jacAD = computeAD(state, tau);
		auto jacNumDiff = computeNumDiff(state, tau);
		auto jacAnalytic = computeAnalytic(state, tau);
		auto jacCodegen = computeQddTauCodegen(state.eigen(), tau);

		if (!jacAD.isApprox(jacNumDiff, 1e-6))
		{
			std::cout << "Jacobians at test " << i << " not equal!"<<std::endl;
			std::cout << "jac num diff: "<<std::endl<<jacNumDiff<<std::endl<<std::endl;
			std::cout << "jac AD: "<<std::endl<<jacAD<<std::endl<<std::endl;
			std::cout << "diff: "<<std::endl<<jacAD-jacNumDiff<<std::endl<<std::endl;
			std::cout << std::endl<<std::endl;
			ASSERT_TRUE(false);
		}

		if (!jacAD.isApprox(jacAnalytic, 1e-12))
		{
			std::cout << "Jacobians at test " << i << " not equal!"<<std::endl;
			std::cout << "jac analytic: "<<std::endl<<jacAnalytic<<std::endl<<std::endl;
			std::cout << "jac AD: "<<std::endl<<jacAD<<std::endl<<std::endl;
			std::cout << "diff: "<<std::endl<<jacAD-jacAnalytic<<std::endl<<std::endl;
			std::cout << std::endl<<std::endl;
			ASSERT_TRUE(false);
		}

		if (!jacAD.isApprox(jacCodegen, 1e-12))
		{
			std::cout << "Jacobians at test " << i << " not equal!"<<std::endl;
			std::cout << "jac codegen: "<<std::endl<<jacCodegen<<std::endl<<std::endl;
			std::cout << "jac AD: "<<std::endl<<jacAD<<std::endl<<std::endl;
			std::cout << "diff: "<<std::endl<<jacAD-jacCodegen<<std::endl<<std::endl;
			std::cout << std::endl<<std::endl;
			ASSERT_TRUE(false);
		}

		precisionAD += (jacAD-jacAnalytic).norm() / nTests;
		precisionNumDiff += (jacNumDiff-jacAnalytic).norm() / nTests;
		precisionCodegen += (jacCodegen-jacAnalytic).norm() / nTests;
	}

	std::cout << "Measured accuracy over "<<nTests<<" tests."<<std::endl;
	std::cout << "mean norm error AD/analytic: "<<precisionAD<<std::endl;
	std::cout << "mean norm error NumDiff/analytic: "<<precisionNumDiff<<std::endl;
	std::cout << "mean norm error Codegen/analytic: "<<precisionCodegen<<std::endl;
}


int main(int argc, char **argv){
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

