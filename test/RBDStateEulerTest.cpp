/*
 * RBDStateEulerTest.cpp
 *
 *  Created on: Aug 10, 2016
 *      Author: neunertm
 */

#include <gtest/gtest.h>

#include <hyq_gen_ad/RBDStateEuler.hpp>

using namespace hyq_gen_ad;

TEST(RBDStateEulerTest, gravityVectorTest)
{
	Eigen::Vector3d gravityB;
	Eigen::Vector3d gravityW;
	gravityW << 0.0, 0.0, -9.81;

	Eigen::Vector3d gravityNegY;
	gravityNegY << 0.0, -9.81, 0.0;

	Eigen::Vector3d gravityNegX;
	gravityNegX << -9.81, 0.0, 0.0;

	RBDStateHyQ state;

	const size_t ntests = 100;

	for (size_t i=0; i<ntests; i++)
	{
		state.eigen().setRandom();
		gravityB.setRandom();

		// set zero orientation and expect gravity to be aligned with world
		state.getBaseOrientationXyz().setZero();

		gravityB = state.computeGravityB();
		if(!gravityW.isApprox(gravityB, 1e-10))
		{
			std::cout << "expected gravity vector: "<<gravityW.transpose()<<std::endl;
			std::cout << "obtained gravity vector: "<<gravityB.transpose()<<std::endl;

			ASSERT_TRUE(false);
		}

		// change yaw and expect gravity to still be aligned with world
		state.getBaseOrientationXyz().setRandom();
		state.getBaseOrientationXyz()(0) = 0.0;
		state.getBaseOrientationXyz()(1) = 0.0;
		gravityB = state.computeGravityB();
		if(!gravityW.isApprox(gravityB, 1e-12))
		{
			std::cout << "expected gravity vector: "<<gravityNegY.transpose()<<std::endl;
			std::cout << "obtained gravity vector: "<<gravityB.transpose()<<std::endl;

			ASSERT_TRUE(false);
		}

		// set roll to +90° and expect gravity in negative y
		state.getBaseOrientationXyz()(0) = M_PI/2.0;
		state.getBaseOrientationXyz()(2) = 0.0;
		gravityB = state.computeGravityB();
		if(!gravityNegY.isApprox(gravityB, 1e-12))
		{
			std::cout << "expected gravity vector: "<<gravityNegY.transpose()<<std::endl;
			std::cout << "obtained gravity vector: "<<gravityB.transpose()<<std::endl;

			ASSERT_TRUE(false);
		}

		// set roll to +90° and yaw to +90° and expect gravity in negative x
		state.getBaseOrientationXyz()(0) = M_PI/2.0;
		state.getBaseOrientationXyz()(2) = M_PI/2.0;
		gravityB = state.computeGravityB();
		if(!gravityNegX.isApprox(gravityB, 1e-12))
		{
			std::cout << "expected gravity vector: "<<gravityNegX.transpose()<<std::endl;
			std::cout << "obtained gravity vector: "<<gravityB.transpose()<<std::endl;

			ASSERT_TRUE(false);
		}
	}
}


TEST(RBDStateEulerTest, accessorsTest)
{
	RBDStateHyQ state;
	RBDStateHyQ testVector;

	const size_t ntests = 100;

	for (size_t i=0; i<ntests; i++)
	{
		state.eigen().setRandom();
		testVector.eigen().setRandom();

		testVector.eigen() << state.getBaseOrientationXyz(), state.getBasePositionW(), state.getJointPositions(),
			state.getBaseLocalAngularVelocityB(), state.getBaseLinearVelocityB(), state.getJointVelocities();

		ASSERT_TRUE(state.eigen() == testVector.eigen());
	}
}



int main(int argc, char **argv){
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
