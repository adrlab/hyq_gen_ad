/*
 * RBDStateEulerTest.cpp
 *
 *  Created on: Aug 10, 2016
 *      Author: neunertm
 */

#include <gtest/gtest.h>

#include <hyq_gen_ad.hpp>

using namespace hyq_gen_ad;

typedef EEKinematics::LEG_ID LEG_ID;

// Test influence of base angular velocity on feet
TEST(EEKinematicsTest, testFootVelocityBaseAngularVelocity)
{
	RBDStateHyQ state;
	iit::HyQ::HomogeneousTransforms transforms;
	iit::HyQ::Jacobians jacobians;
	EEKinematics eeKinematics(transforms,jacobians);

	// straight pose, all legs streched
	state.eigen().setZero();

	// rotate around x
	state.getBaseLocalAngularVelocityB()(0) = 1.3;

	const size_t nFeet = 4;
	for (size_t i=0; i<nFeet; i++)
	{
		Eigen::Vector3d eeVelW;
		Eigen::Vector3d eeVelB;
		eeKinematics.getEEVelInWorld(eeVelW, static_cast<LEG_ID>(i), state);
		eeKinematics.getEEVelInBase(eeVelB, static_cast<LEG_ID>(i), state);

		// Since world and base are aligned, both velocities should be the same
		ASSERT_TRUE(eeVelW.isApprox(eeVelB));

		// x component should be zero
		ASSERT_NEAR(eeVelW(0), 0.0, 1e-6);

		// y component should be greater zero
		ASSERT_GT(eeVelW(1), 0.0);
	}
}


// Test influence of base angular velocity on feet
TEST(EEKinematicsTest, testFootVelocityBaseZRotation)
{
	RBDStateHyQ state;
	iit::HyQ::HomogeneousTransforms transforms;
	iit::HyQ::Jacobians jacobians;
	EEKinematics eeKinematics(transforms,jacobians);

	// random configuration
	state.eigen().setRandom();

	// straight orientation, no joint velocity
	state.getBaseOrientationXyz().setZero();
	state.getJointVelocities().setZero();

	// rotate around z only
	state.getBaseLocalAngularVelocityB()(0) = 0.0;
	state.getBaseLocalAngularVelocityB()(1) = 0.0;
	state.getBaseLocalAngularVelocityB()(2) = 1.3;

	const size_t nFeet = 4;
	for (size_t i=0; i<nFeet; i++)
	{
		Eigen::Vector3d eeVelW;
		Eigen::Vector3d eeVelB;
		eeKinematics.getEEVelInWorld(eeVelW, static_cast<LEG_ID>(i), state);
		eeKinematics.getEEVelInBase(eeVelB, static_cast<LEG_ID>(i), state);

		// Since world and base are aligned, both velocities should be the same
		ASSERT_TRUE(eeVelW.isApprox(eeVelB));

		// z component should be linear velocity in z
		ASSERT_NEAR(eeVelW(2), state.getBaseLinearVelocityB()(2), 1e-6);
	}
}


// Test influence of base linear velocity on feet
TEST(EEKinematicsTest, testFootVelocityBaseLinearVelocity)
{
	RBDStateHyQ state;
	iit::HyQ::HomogeneousTransforms transforms;
	iit::HyQ::Jacobians jacobians;
	EEKinematics eeKinematics(transforms,jacobians);


	const size_t nTests = 1000;

	for(size_t t=0; t<nTests; t++)
	{
		// random configuration
		state.eigen().setRandom();

		// straight orientation, no rotational velocity
		state.getBaseOrientationXyz().setZero();
		state.getBaseLocalAngularVelocityB().setZero();
		state.getJointVelocities().setZero();

		const size_t nFeet = 4;
		for (size_t i=0; i<nFeet; i++)
		{
			Eigen::Vector3d eeVelW;
			Eigen::Vector3d eeVelB;
			eeKinematics.getEEVelInWorld(eeVelW, static_cast<LEG_ID>(i), state);
			eeKinematics.getEEVelInBase(eeVelB, static_cast<LEG_ID>(i), state);

			// Since world and base are aligned, both velocities should be the same
			ASSERT_TRUE(eeVelW.isApprox(eeVelB));

			// Velocity of feet should be equal to velocity of base
			ASSERT_TRUE(eeVelW.isApprox(state.getBaseLinearVelocityB()));
		}
	}
}


// Test foot positions in base
TEST(EEKinematicsTest, testFootPositionVaryingBase)
{
	RBDStateHyQ state;
	iit::HyQ::HomogeneousTransforms transforms;
	iit::HyQ::Jacobians jacobians;
	EEKinematics eeKinematics(transforms,jacobians);


	const size_t nTests = 1000;

	for(size_t t=0; t<nTests; t++)
	{
		// random configuration
		state.eigen().setRandom();

		state.getJointPositions().setZero();

		const size_t nFeet = 4;
		std::array<Eigen::Vector3d, nFeet> B_eePos;
		for (size_t i=0; i<nFeet; i++)
		{
			 eeKinematics.getEEPosInBase(B_eePos[i], static_cast<LEG_ID>(i), state.getJointPositions());

			 // all legs should have same height
			if (i>0)
				ASSERT_NEAR(B_eePos[0](2), B_eePos[i](2), 1e-6);

			// legs should be below belly
			ASSERT_LT(B_eePos[0](2), -0.5);
			ASSERT_GT(B_eePos[0](2), -1.5);
		}
	}
}


// Test influence of base linear velocity on feet
TEST(EEKinematicsTest, testFootPositionStraightBase)
{
	RBDStateHyQ state;
	iit::HyQ::HomogeneousTransforms transforms;
	iit::HyQ::Jacobians jacobians;
	EEKinematics eeKinematics(transforms,jacobians);


	const size_t nTests = 1000;

	for(size_t t=0; t<nTests; t++)
	{
		// random configuration
		state.eigen().setRandom();

		// straight orientation, no rotational velocity
		state.getBaseOrientationXyz().setZero();

		state.getJointPositions().setZero();

		const size_t nFeet = 4;
		std::array<Eigen::Vector3d, nFeet> W_eePos;
		for (size_t i=0; i<nFeet; i++)
		{
			 eeKinematics.getEEPosInWorld(W_eePos[i], static_cast<LEG_ID>(i), state);

			 // all legs should have same height
			if (i>0)
				ASSERT_NEAR(W_eePos[0](2), W_eePos[i](2), 1e-6);

			// legs should be below belly
			ASSERT_LT(W_eePos[0](2), state.getBasePositionW()(2)-0.5);
			ASSERT_GT(W_eePos[0](2), state.getBasePositionW()(2)-1.5);
		}
	}
}


void checkExtForcesZeroNotLowerLeg(const EEKinematics::ExtForces& extForces)
{
	for (size_t i=0; i<13; i++)
	{
		if (i<2 && (i-1)%3 != 0)
		{
			iit::HyQ::LinkIdentifiers id = static_cast<iit::HyQ::LinkIdentifiers>(i);
			ASSERT_NEAR(extForces[id].norm(), 0.0, 1e-12);
		}
	}
}


// Test influence of base linear velocity on feet
TEST(EEKinematicsTest, forceMappingTest)
{
	RBDStateHyQ state;
	iit::HyQ::HomogeneousTransforms transforms;
	iit::HyQ::Jacobians jacobians;
	EEKinematics eeKinematics(transforms,jacobians);


	const size_t nTests = 1000;

	for(size_t t=0; t<nTests; t++)
	{
		// random configuration
		state.eigen().setRandom();

		// straight orientation, no rotational velocity
		state.getBaseOrientationXyz().setZero();

		state.getJointPositions().setZero();

		const size_t nFeet = 4;

		std::array<Eigen::Vector3d, nFeet> eeForcesW;


		for (size_t i=0; i<nFeet; i++)
		{
			// only force in z
			eeForcesW[i].setZero();
			eeForcesW[i].segment<1>(2).setRandom();
		}

		EEKinematics::ExtForces extForces(Eigen::Matrix<double, 6, 1>::Zero());

		eeKinematics.mapEEForcesToExtForces(state, eeForcesW, extForces);

		for (size_t i=0; i<nFeet; i++)
		{
			// we only expect forces in negative x, no torques
			for (size_t j=0; j<6; j++)
			{
				if (j!=3)
					ASSERT_NEAR(extForces[static_cast<iit::HyQ::LinkIdentifiers>(3*(i+1))](j), 0.0, 1e-6);
			}

			ASSERT_NEAR(extForces[static_cast<iit::HyQ::LinkIdentifiers>(3*(i+1))](3), -eeForcesW[i](2), 1e-6);
		}
	}
}



// Test influence of base linear velocity on feet
TEST(EEKinematicsTest, forceMagnitudeTest)
{
	RBDStateHyQ state;
	iit::HyQ::HomogeneousTransforms transforms;
	iit::HyQ::Jacobians jacobians;
	EEKinematics eeKinematics(transforms,jacobians);

	const size_t nTests = 1000;

	for(size_t t=0; t<nTests; t++)
	{
		// random configuration
		state.eigen().setRandom();

		const size_t nFeet = 4;
		std::array<Eigen::Vector3d, nFeet> eeForcesW;

		for (size_t i=0; i<nFeet; i++)
		{
			eeForcesW[i].setRandom();
		}

		EEKinematics::ExtForces extForces(Eigen::Matrix<double, 6, 1>::Zero());
		eeKinematics.mapEEForcesToExtForces(state, eeForcesW, extForces);

		for (size_t i=0; i<nFeet; i++)
		{
			// we expect the magnitude not to change
			ASSERT_NEAR(extForces[static_cast<iit::HyQ::LinkIdentifiers>(3*(i+1))].bottomRows<3>().norm(), eeForcesW[i].norm(), 1e-6);
		}
	}
}



// Test influence of base linear velocity on feet
TEST(EEKinematicsTest, torqueMappingTest)
{
	RBDStateHyQ state;
	iit::HyQ::HomogeneousTransforms transforms;
	iit::HyQ::Jacobians jacobians;
	EEKinematics eeKinematics(transforms,jacobians);


	const size_t nTests = 1000;

	for(size_t t=0; t<nTests; t++)
	{
		// random configuration
		state.eigen().setRandom();

		// straight orientation, no rotational velocity
		state.getBaseOrientationXyz().setZero();

		state.getJointPositions().setZero();

		const size_t nFeet = 4;

		std::array<Eigen::Vector3d, nFeet> eeForcesW;


		for (size_t i=0; i<nFeet; i++)
		{
			state.getJointPositions()(3*i+2) = M_PI/2.0; // knees bent by 90°

			// only force in z
			eeForcesW[i].setZero();
			eeForcesW[i].segment<1>(2).setRandom();
		}

		EEKinematics::ExtForces extForces(Eigen::Matrix<double, 6, 1>::Zero());

		eeKinematics.mapEEForcesToExtForces(state, eeForcesW, extForces);

		for (size_t i=0; i<nFeet; i++)
		{
			// we only expect forces in negative y
			ASSERT_NEAR(extForces[static_cast<iit::HyQ::LinkIdentifiers>(3*(i+1))](3), 0.0, 1e-6);
			ASSERT_NEAR(extForces[static_cast<iit::HyQ::LinkIdentifiers>(3*(i+1))](4), eeForcesW[i](2),1e-6);
			ASSERT_NEAR(extForces[static_cast<iit::HyQ::LinkIdentifiers>(3*(i+1))](5), 0.0, 1e-6);

			// we expect torques only around z
			for (size_t j=0; j<2; j++)
			{
				ASSERT_NEAR(extForces[static_cast<iit::HyQ::LinkIdentifiers>(3*(i+1))](j), 0.0, 1e-6);
			}

			double torqueAbs = std::abs(extForces[static_cast<iit::HyQ::LinkIdentifiers>(3*(i+1))](2));
			ASSERT_NEAR(torqueAbs, eeForcesW[i].norm()*0.33, 1e-6);
		}

		checkExtForcesZeroNotLowerLeg(extForces);
	}
}





int main(int argc, char **argv){
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

