/*
 * test.cpp
 *
 *  Created on: Aug 9, 2016
 *      Author: neunertm
 */

#define ROBCOGEN_AD_NAMESPACE CppAD

#include <gtest/gtest.h>

#include <hyq_gen_ad.hpp>

using namespace iit::rbd;
using namespace iit::HyQ;
using namespace hyq_gen_ad;

typedef RBDStateEuler<12, CppAD::AD<double> > RBDStateHyQAD;
typedef RBDStateEuler<12, float> RBDStateHyQF;

JointState computeNormal(
	const RBDStateHyQ& state,
	const JointState& tau)
{
	dyn::InertiaProperties inertiaProperties;
	MotionTransforms motionTransforms;

	dyn::ForwardDynamics fd(inertiaProperties, motionTransforms);

	JointState qdd;
	VelocityVector trunk_a;

	LinkDataMap<iit::rbd::ForceVector> fext;
	for (size_t i=0; i<13; i++)
		fext[iit::HyQ::LinkIdentifiers(i)].setZero();

	fd.fd(	qdd, trunk_a,
			state.convertBaseVelocityBPluecker(),
			state.computeGravityBPluecker(),
			state.getJointPositions(),
			state.getJointVelocities(),
			tau, fext);

	return qdd;
}

JointState computeFloat(
	const RBDStateHyQ& state,
	const JointState& tau)
{
	dyn::InertiaProperties inertiaProperties;
	MotionTransformsTpl<FloatTrait> motionTransforms;

	dyn::ForwardDynamicsTpl<FloatTrait> fd(inertiaProperties, motionTransforms);

	JointStateTpl<float> qdd;
	VelocityVectorTpl<float> trunk_a;

	RBDStateHyQF stateF(state.eigen().cast<float>());

	JointStateTpl<float> tauF = tau.cast<float>();

	LinkDataMap<iit::rbd::ForceVectorTpl<float>> fextF;
	for (size_t i=0; i<13; i++)
			fextF[iit::HyQ::LinkIdentifiers(i)].setZero();

	fd.fd(	qdd, trunk_a,
			stateF.convertBaseVelocityBPluecker(),
			stateF.computeGravityBPluecker(),
			stateF.getJointPositions(),
			stateF.getJointVelocities(),
			tauF, fextF);

	return qdd.cast<double>();
}

JointState computeAD(
	const RBDStateHyQ& state,
	const JointState& tau)
{
	typedef CppAD::AD<double> doubleAD;

	dyn::InertiaProperties inertiaProperties;
	MotionTransformsTpl<CppADDoubleTrait> motionTransforms;

	dyn::ForwardDynamicsTpl<CppADDoubleTrait> fd(inertiaProperties, motionTransforms);

	JointStateTpl<doubleAD> qdd;
	VelocityVectorTpl<doubleAD> trunk_a;

	RBDStateHyQAD stateAD(state.eigen().cast<doubleAD>());
	JointStateTpl<doubleAD> tauAD = tau.cast<doubleAD>();

	LinkDataMap<iit::rbd::ForceVectorTpl<doubleAD>> fextAD;
	for (size_t i=0; i<13; i++)
			fextAD[iit::HyQ::LinkIdentifiers(i)].setZero();

	fd.fd(	qdd, trunk_a,
			stateAD.convertBaseVelocityBPluecker(),
			stateAD.computeGravityBPluecker(),
			stateAD.getJointPositions(),
			stateAD.getJointVelocities(),
			tauAD, fextAD);


	JointState qdd_double;
	for (size_t i=0; i<JointState::RowsAtCompileTime; i++)
	{
		qdd_double(i) = CppAD::Value(qdd(i));
	}

	return qdd_double;
}



TEST(evaluationTest, outputComparisonTest)
{
	RBDStateHyQ state;
	JointState tau;

	JointState qddNormal;
	JointState qddFloat;
	JointState qddAD;

	const size_t nTests = 10000;

	for (size_t i=0; i<nTests; i++)
	{
		state.eigen().setRandom();
		tau.setRandom();

		qddNormal = computeNormal(state, tau);
		qddFloat = computeFloat(state, tau);
		qddAD = computeAD(state, tau);

		if (!qddNormal.isApprox(qddAD, 1e-12))
		{
			std::cout << "qdd double: "<<qddNormal.transpose()<<std::endl;
			std::cout << "qdd AD: "<<qddAD.transpose()<<std::endl;
			std::cout << "at test " << i << " not equal!"<<std::endl;
			ASSERT_TRUE(false);
		}

		if (!qddNormal.isApprox(qddFloat, 1e-6))
		{
			std::cout << "qdd double: "<<qddNormal.transpose()<<std::endl;
			std::cout << "qdd float: "<<qddFloat.transpose()<<std::endl;
			std::cout << "at test " << i << " not equal!"<<std::endl;
			ASSERT_TRUE(false);
		}
	}
}


int main(int argc, char **argv){
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

